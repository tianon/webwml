#use wml::debian::template title="Wsparcie"
#use wml::debian::toc
#use wml::debian::translation-check translation="6838e6aa35cea0dd360ea9a9f08965ebdc8c9d50"

<define-tag toc-title-formatting endtag="required" whitespace="delete">
<h2>%body</h2>
</define-tag>

Debian i jego wsparcie są prowadzone przez społeczność wolontariuszy.

Jeżeli to wsparcie społecznościowe nie spełnia Twoich potrzeb, możesz 
przeczytać naszą <a href="doc/">dokumentację</a> lub zatrudnić 
<a href="consultants/">konsultanta</a>.

<toc-display/>

<toc-add-entry name="irc">Pomoc w czasie rzeczywistym przez IRC</toc-add-entry>

<p><a href="http://www.irchelp.org/">IRC (Internet Relay Chat)</a> to sposób 
na porozmawianie z ludźmi z całego świata w czasie rzeczywistym. 
Kanały IRC poświęcone Debianowi można znaleźć na stronie 
<a href="https://www.oftc.net/">OFTC</a>.</p>

<p>Aby połączyć się z serwerem IRC, potrzebny jest specjalny program: klient
IRC. Najpopularniejsze z nich to <a
href="https://packages.debian.org/stable/net/hexchat">HexChat</a>, <a
href="https://packages.debian.org/stable/net/ircii">ircII</a>, <a
href="https://packages.debian.org/stable/net/irssi">irssi</a>, <a
href="https://packages.debian.org/stable/net/epic5">epic5</a> oraz <a
href="https://packages.debian.org/stable/net/kvirc">KVIrc</a> - 
istnieją pakiety Debiana zawierające każdy z nich. OFTC oferuje także 
<a href="https://www.oftc.net/WebChat/">WebChat</a>, który umożliwia 
połączenie się do IRC poprzez przeglądarkę internetową bez konieczności 
instalowania jakiegokolwiek klienta lokalnego.</p>

<p>Po zainstalowaniu pakietu musisz połączyć się z naszym serwerem. 
W przypadku większości klientów IRC można to zrobić wpisując:</p>

<pre>
/server irc.debian.org
</pre>

<p>W niektórych klientach (takich jak irssi) zamiast tego należy wpisać:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.

# <p>Po połączeniu wejdź na kanał <code>#debian-FOO</code> wpisując</p>
# <pre>/join #debian-FOO</pre>
# aby uzyskać pomoc w języku polskim.
# <p>Aby uzyskać pomoc w języku angielskim, ...</p>

<p>Po połączeniu wejdź na kanał <code>#debian</code> wpisując</p>

<pre>
/join #debian
</pre>

<p>Uwaga: klienty takie jak HexChat działają inaczej - posiadają intuicyjny
interfejs graficzny umożliwiający łączenie się z serwerami i wchodzenie na
kanały.</p>

<p>W tym momencie znajdziesz się w przyjaznym gronie użytkowników kanału
<code>#debian</code>. Zapraszamy do zadawania pytań. Najczęściej
zadawane pytania znajdują się pod adresem 
<url "https://wiki.debian.org/DebianIRC">.</p>


<p>Dostępnych jest też wiele innych sieci IRC, na których można porozmawiać
na temat Debiana. Jedną z najważniejszych jest
<a href="https://freenode.net/">freenode IRC network</a> na
<kbd>chat.freenode.net</kbd>.</p>

<toc-add-entry name="mail_lists" href="MailingLists/">Listy dyskusyjne</toc-add-entry>

<p>Debian jest rozwijany przez ludzi rozproszonych po całym
świecie. Dlatego poczta elektroniczna jest najpopularniejszym sposobem na dyskutowanie na
różne tematy. Duża część komunikacji między twórcami a użytkownikami Debiana
odbywa się za pomocą kilku list dyskusyjnych.</p>

<p>Istnieje kilka publicznie dostępnych list dyskusyjnych. Więcej informacji
można znaleźć na <a href="MailingLists/">stronie list dyskusyjnych
Debiana</a>.</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<p>
Użytkownicy Debiana mogą uzyskać pomoc w języku polskim na liście dyskusyjnej
<a href="https://lists.debian.org/debian-user-polish/">debian-user-polish</a>.
</p>

<p>
Dostępna jest też pomoc w innych językach &mdash; patrz
<a href="https://lists.debian.org/users.html">spis list dyskusyjnych dla użytkowników</a>.
</p>

<p>Istnieje również wiele innych list dyskusyjnych poświęconych jakiemuś
aspektowi rozległego ekosystemu Linuksa, które nie są związane z Debianem.
Twoja ulubiona wyszukiwarka pomoże Ci znaleźć najbardziej odpowiednią dla
Twojego zagadnienia listę.</p>


<toc-add-entry name="usenet">Grupy dyskusyjne</toc-add-entry>

<p>Wiele naszych <a href="#mail_lists">list dyskusyjnych</a> można przeglądać
jako grupy dyskusyjne w hierarchii <kbd>linux.debian.*</kbd>. Są one także
dostępne przez interfejsy WWW takie jak 
<a href="https://groups.google.com/forum/">Google Groups</a>.</p>

<toc-add-entry name="web">Strony www</toc-add-entry>

<h3 id="forums">Fora</h3>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.

<p>Pod adresem <a href="http://www.debian.pl">http://debian.pl/</a>
znajduje się polskojęzyczne (nieoficjalne) forum dla użytkowników systemu
Debian.</p>

<p>Z angielskojęzycznych warto zobaczyć portal 
<a href="http://forums.debian.net">Debian User Forums</a>, 
na którym można prowadzić dyskusje na tematy związane z Debianem,
zamieszczać pytania na jego temat i uzyskać na nie
odpowiedzi od innych użytkowników.</p>

<toc-add-entry name="maintainers">Kontakt z opiekunami pakietów</toc-add-entry>

<p>Istnieją dwa sposoby na skontaktowanie się z opiekunem pakietu. Jeśli
chcesz się z nim/nią skontaktować z powodu błędu w pakiecie, po prostu
wyślij zgłoszenie błędu (patrz system śledzenia błędów, poniżej). Opiekun
pakietu otrzyma kopię tego zgłoszenia.</p>

<p>Jeśli chcesz tylko skontaktować się z opiekunem, możesz użyć specjalnego
aliasu stworzonego dla każdego pakietu. Poczta wysyłana na adres
&lt;<em>nazwa pakietu</em>&gt;@packages.debian.org zostanie przekazana do
osoby odpowiedzialnej za dany pakiet.</p>


<toc-add-entry name="bts" href="Bugs/">System śledzenia błędów</toc-add-entry>

<P>Dystrybucja Debian posiada system śledzenia błędów, który
gromadzi zgłoszenia błędów przysyłane przez użytkowników i twórców
dystrybucji. Każdy błąd posiada numer i jest przechowywany do czasu jego
naprawienia.</p>

<P>Błąd można zgłosić przy pomocy którejś ze specjalnych stron podanych
poniżej albo pakietu Debiana <q>reportbug</q>, który służy do automatycznego
wypełniania raportu o błędach.</p>

<P>Informacje na temat wysyłania zgłoszeń błędów, przeglądania istniejących
błędów i systemu ich śledzenia w ogólności można znaleźć na <a
href="Bugs/">jego stronach www</a>.</p>


<toc-add-entry name="consultants" href="consultants/">Konsultanci</toc-add-entry>

<P>Debian to wolne oprogramowanie - oferujemy pomoc przez listy
dyskusyjne. Niektórzy jednak nie mają czasu lub mają specjalne wymagania i
są skłonni zapłacić komuś, aby zajmował się ich systemem, lub w jakiś sposób
go rozbudował. Lista ludzi/firm świadczących takie usługi znajduje się na <a
href="consultants/">stronie konsultantów</a>.</p>

<toc-add-entry name="release" href="releases/stable/">Znane problemy</toc-add-entry>

<p>Ograniczenia oraz poważne usterki bieżącej stabilnej dystrybucji 
(jeśli takie zostały zauważone) są opisane na 
<a href="releases/stable/">stronie danego wydania</a>.</p>

<p>Szczególną uwagę należy zwrócić na 
<a href="releases/stable/releasenotes">uwagi dotyczące wydania</a> 
oraz <a href="releases/stable/errata">erratę</a>.</p>

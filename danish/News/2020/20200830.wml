#use wml::debian::translation-check translation="549ed0fce26a03a0a08c5aca5a6c51e4199c3e20"
<define-tag pagetitle>DebConf20 online slutter</define-tag>

<define-tag release_date>2020-08-30</define-tag>
#use wml::debian::news

<p>Lørdag den 29. august 2020 sluttede den årlige Debian Developers and 
Contributors Conference.</p>

<p>DebConf20 blev for første gang afholdt online, på grund af 
coronaviruspandemien (COVID-19).</p>

<p>Alle seancerne blev streamet, med en række forskellige måder at deltage på 
gennem IRC-beskeder, samardejdstekstdokumenter online og 
videokonferencemødelokaler.</p>

<p>Med flere end 850 deltagere fra 80 forskellige lande, og i alt over 100 
foredrag, diskussionsseancer, Birds of a Feather-møder (BoF) og andre 
aktiviteter, blev 
<a href="https://debconf20.debconf.org">DebConf20</a> en stor succes.</p>

<p>Da det stod klart, at DebConf20 kun ville blive en onlinebegivenhed, brugte 
DebConfs videohold megen tid over de følgende månder til at tilpasse, forbedre, 
og i nogle tilfælde skrive fra bunden, teknologi som ville være krævet for at 
gøre en onlineudgave af DebConf mulig.  Efter erfaringer høstet under 
MiniDebConfOnline sidst i maj måned, blev der foretaget nogle justeringer, og 
slutteligt endte vi med en opsætning som involverede Jitsi, OBS, Voctomix, 
SReview, nginx, Etherpad, samt en nyligt udviklt webbaseret grænseflade til 
voctomix, som de forskellige elemener i det komplette system.</p>

<p>Alle komponenter i videoinfrastrukturen er fri software, og det hele opsættes 
gennem deres offentlige 
<a href="https://salsa.debian.org/debconf-video-team/ansible">\
ansible</a>-arkiv.</p>

<p>DebConf20's <a href="https://debconf20.debconf.org/schedule/">program</a> 
indeholdt to spor på andre sprog end engelsk; den spansksprogede MiniConf med 
otte foredrag på to dage, og den malayalamsprogede MiniConf med ni foredrag på 
tre dage.  Ad-hoc-aktiviteter, indført af deltagere under hele konferencen, var 
også mulige, og blev streamet og optaget.  Der var også flere teammøder for at 
<a href="https://wiki.debian.org/Sprints/">sprinte</a> på visse af Debians 
udviklingsområder.</p>

<p>Mellem foredragene, viste videostrømmene de sædvanlige sponsorer i en løkke, 
men også nogle yderligere klip med billeder fra tidligere DebConf'er, sjove 
fakta om Debian og korte shout-out-videoer sendt af deltagerne for at 
kommunikere med deres Debian-venner.</p>

<p>Til dem der ikke var i stand til at deltage, er de fleste fordrag og seancer 
allerede tilgængelige på 
<a href="https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/">\
Debians mødearkivwebsted</a>, og de resterende vil dukke op i de kommende 
dage.</p>

<p><a href="https://debconf20.debconf.org/">DebConf20</a>-webstedet vil forblive 
aktivt af historiske årsager, og vil fortsat indeholde links til præsentationer 
og videoer fra foredrag og begivenheder.</p>

<p>Næste år er <a href="https://wiki.debian.org/DebConf/21">DebConf21</a> 
planlagt til at finde sted i Haifa, Israel, i august eller september.</p>

<p>DebConf har forpligtet sig til at være et sikkert og imødekommende miljø for 
alle deltagere.  Under konferencen er flere hold (forkontor, velkomst og 
antichikane) til stede for at hjælpe med at både tilstedeværende og fjerne 
deltagere får den bedste konferenceoplevelse, og finder løsninger på alle 
problemer, der måtte opstå.  Se 
<a href="https://debconf20.debconf.org/about/coc/">websiden om de etiske 
retningslinjer på DebConf20's websted</a>, for flere oplysninger herom.</p>

<p>Debian takker de talrige 
<a href="https://debconf20.debconf.org/sponsors/">sponsorer</a> for støtten 
til DebConf20, i særdeleshed vores platinsponsorer 
<a href="https://www.lenovo.com">Lenovo</a>,
<a href="https://www.infomaniak.com">Infomaniak</a>,
<a href="https://google.com/">Google</a> og 
<a href="https://aws.amazon.com/">Amazon Web Services (AWS)</a>.</p>


<h2>Om Debian</h2>

<p>Debian-projektet blev grundlagt i 1993 af Ian Murdock, som et helt frit 
fællesskabsprojekt.  Siden den gang, er projektet vokset til at være et af de 
største og mest indflydelsesrige open source-projekter.  Tusindvis af
frivillige fra hele verden samarbejder om at fremstille og vedligeholde 
Debian-software.  Med oversættelser til 70 sprog, og med understøttelse af et 
enormt antal computertyper, kalder Debian sig det <q>universelle 
styresystem</q>.</p>


<h2>Om DebConf</h2>

<p>DebConf er Debian-projektets udviklerkonference.  Ud over et fuldt program 
med tekniske, sociale og retningslinjeforedrag, gør DebConf det også muligt 
at udviklere, bidragydere og andre interesserede kan mødes personligt og 
arbejde tættere sammen.  Konferencen har fundet sted siden år 2000 i så 
forskellige steder som Skotland, Argentina og Bosien-Hercegovina.  Flere 
oplysninger er tilgængelige på <a href="http://debconf.org/">\
DebConf-webstedet</a>.</p>


<h2>Om Lenovo</h2>

<p>Som en global teknologileder med fremstilling af en bred portefølje af 
forbundne produkter, herunder smartphones, tablets, PC'er og workstations, 
foruden AR/VR-enheder, smarthome/-kontor og datacenterløsninger, forstår 
<a href="https://www.lenovo.com">Lenovo</a> hvor afgørende åbne systemer og 
platforme er for den forbundne verden.</p>


<h2>Om Infomaniak</h2>

<p><a href="https://www.infomaniak.com">Infomaniak</a> er Schweiz' største 
webhostingvirksomhed, der desuden tilbyder backup- og storagetjenester, 
løsninger til begivenhedsorganisatorer, livestreamning og video on 
demand-tjenster.  De ejer selv deres datacentre og alle elementer, der er 
afgørende for at de tjenester og produkter, som virksomheden tilbyder, 
fungerer (både software og hardware).</p>


<h2>Om Google</h2>

<p><a href="https://google.com/">Google</a> er en af de største 
teknologivirksomheder i verden, og tilbyder et bredt udvalg af 
internetrelaterede tjenester og produkter, så som onlinereklameteknologier, 
søgning, cloudcomputing, software og hardware.</p>

<p>Google har støttet Debian ved at sponsere DebConf i mere end ti år, og er 
desuden en Debian-partner, som sponserer dele af 
<a href="https://salsa.debian.org">Salsa</a>s continuous 
integration-infrastruktur på Googles cloudplatform.</p>


<h2>Om Amazon Web Services (AWS)</h2>

<p><a href="https://aws.amazon.com">Amazon Web Services (AWS)</a> er en af 
verdens mest omfattende og mest benyttede cloudplatforme, som tilbyder over 
175 komplette services fra globale datacenter (i 77 tilgængelige zoner 
fordelt på 24 geografiske områder).  AWS-kunder er blandt andre de hurtigst 
voksende iværksættervirksomheder, de største virksomheder og førende 
myndighedsstyrelser.</p>



<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg DebConf20's websider på
<a href="https://debconf20.debconf.org/">https://debconf20.debconf.org/</a>
eller send en engelsksproget mail til &lt;press@debian.org&gt;.</p>

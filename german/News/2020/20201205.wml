<define-tag pagetitle>Debian 10 aktualisiert: 10.7 veröffentlicht</define-tag>
<define-tag release_date>2020-12-05</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="5c427e44dc4a1503d5262b5edfba60b490ea0ab1" maintainer="Erik Pfannenstein"

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Das Debian-Projekt freut sich, die siebte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction choose-mirror "Liste der Spiegelserver aktualisiert">
<correction cups "Ungültiges free bei 'printer-alert' behoben">
<correction dav4tbsync "Neue Veröffentlichung der Originalautoren, kompatibel mit neueren Thunderbird-Versionen">
<correction debian-installer "Linux-Kernel-ABI 4.19.0-13 verwenden; grub2 zu Built-Using hinzugefügt">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction distro-info-data "Ubuntu 21.04, Hirsute Hippo, hinzugefügt">
<correction dpdk "Neue stabile Veröffentlichung der Originalautoren; Problem mit Code-Fernausführung behoben [CVE-2020-14374], außerdem TOCTOU-Probleme [CVE-2020-14375], Pufferüberlauf [CVE-2020-14376], übermäßiges Lesen des Puffers (buffer over-read) [CVE-2020-14377] und Ganzzahlüberlauf [CVE-2020-14377]; armhf-Kompilierung mit NEON überarbeitet">
<correction eas4tbsync "Neue Veröffentlichung der Originalautoren, kompatibel mit neueren Thunderbird-Versionen">
<correction edk2 "Ganzzahlüberlauf beim DxeImageVerificationHandler beseitigt [CVE-2019-14562]">
<correction efivar "Unterstützung für NVMe-Fabrics und NVMe-Subsystem-Geräte hinzugefügt; uninitialisierte Variable in parse_acpi_root korrigiert, um möglichen Speicherzugriffsfehler zu beheben">
<correction enigmail "Migrationsassistenten für Thunderbirds eingebaute GPG-Unterstützung eingeführt">
<correction espeak "Verwendung von espeak mit mbrola-fr4 für den Fall angepasst, dass mbrola-fr1 nicht installiert ist">
<correction fastd "Speicherleck behoben, das auftritt, wenn zu viele ungültige Pakete eingegangen sind [CVE-2020-27638]">
<correction fish "Sicherstellen, dass die TTY-Optionen beim Verlassen wiederhergestellt werden">
<correction freecol "Anfälligkeit für externe XML-Entitäten behoben [CVE-2018-1000825]">
<correction gajim-omemo "12-Byte-IV verwenden, um die Kompatibilität mit iOS-Clients zu verbessern">
<correction glances "Standardmäßig nur bei Localhost lauschen">
<correction iptables-persistent "Laden von Kernel-Modulen nicht erzwingen; Regel-Leerungs-Logik überarbeitet">
<correction lacme "Statt der hartkodierten die von den Originalautoren gelieferte Zertifikatskette benutzen, um die Unterstützung von neuen Let's-Encrypt-Wurzel- und Zwischenzertifikaten zu erleichtern">
<correction libdatetime-timezone-perl "Enthaltene Daten auf tzdata 2020d aktualisiert">
<correction libimobiledevice "Teilweise Unterstützung für iOS 14 hinzugefügt">
<correction libjpeg-turbo "Dienstblockade [CVE-2018-1152], übermäßiges Lesen des Puffers (buffer over-read) [CVE-2018-14498], mögliche Fernausführung von Code [CVE-2019-2201] und übermäßiges Lesen des Puffers (buffer over-read) [CVE-2020-13790] behoben">
<correction libxml2 "Dienstblockade [CVE-2017-18258], Nullzeiger-Dereferenzierung [CVE-2018-14404], Endlosschleife [CVE-2018-14567], Speicherleck [CVE-2019-19956 CVE-2019-20388], Endlosschleife [CVE-2020-7595] behoben">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Aktualisierung auf 4.19.0-13er Kernel-ABI">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction lmod "Architektur auf <q>any</q> umgestellt - nötig, weil LUA_PATH und LUA_CPATH während der Kompilierung ermittelt werden">
<correction mariadb-10.3 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-14765 CVE-2020-14776 CVE-2020-14789 CVE-2020-14812 CVE-2020-28912]">
<correction mutt "Sichergehen, dass die IMAP-Verbindung nach einem Verbindungsfehler geschlossen wird [CVE-2020-28896]">
<correction neomutt "Sichergehen, dass die IMAP-Verbindung nach einem Verbindungsfehler geschlossen wird [CVE-2020-28896]">
<correction node-object-path "Prototype-Pollution in set() behoben [CVE-2020-15256]">
<correction node-pathval "Prototype-Pollution behoben [CVE-2020-7751]">
<correction okular "Codeausführung via Aktionsverküpfung behoben [CVE-2020-9359]">
<correction openjdk-11 "Neue Veröffentlichung der Originalautoren; JVM-Absturz behoben">
<correction partman-auto "Größen von /boot in den meisten Rezepten auf Werte zwischen 512 und 768M angehoben, um besser mit Kernel-ABI-Änderungen und größeren initramfsen umgehen zu können; RAM-Größe, welche für die Berechnung der Swap-Partition herangezogen wird, deckeln, um Probleme auf Maschinen, die mehr RAM als Festspeicher haben, zu lösen">
<correction pcaudiolib "Unterdrückungslatenz auf 10ms deckeln">
<correction plinth "Apache: mod_status deaktiviert [CVE-2020-25073]">
<correction puma "HTTP-Injektions- und HTTP-Schmuggel-Problme behoben [CVE-2020-5247 CVE-2020-5249 CVE-2020-11076 CVE-2020-11077]">
<correction ros-ros-comm "Ganzzahlüberlauf behoben [CVE-2020-16124]">
<correction ruby2.5 "Potenzielle Anfälligkeit für HTTP-Anfrageschmuggel in WEBrick behoben [CVE-2020-25613]">
<correction sleuthkit "Stapelpufferüberlauf in yaffsfs_istat behoben [CVE-2020-10232]">
<correction sqlite3 "Teilung durch null [CVE-2019-16168], Nullzeiger-Dereferenzierung [CVE-2019-19923], Falschbehandlung von NULL-Pfadnamen während einer Aktualisierung eines ZIP-Archivs [CVE-2019-19925], Falschbehandlung von NULen in Dateinamen [CVE-2019-19959], möglichen Absturz (WITH-Stack-Unwinding) [CVE-2019-20218], Ganzzahlüberlauf [CVE-2020-13434], Speicherzugriffsfehler [CVE-2020-13435], use-after-free-Problem [CVE-2020-13630], Nullzeiger-Dereferenzierung [CVE-2020-13632], Heap-Überlauf [CVE-2020-15358] behoben">
<correction systemd "Basic/cap-list: numerische Leistungsmerkmale auswerten/ausgeben; neue Leistungsmerkmale des Linux-Kernel 5.8 erkennen; networkd: keine MAC für Brückengeräte erzeugen">
<correction tbsync "Neue Veröffentlichung der Originalautoren, kompatibel mit neueren Thunderbird-Versionen">
<correction tcpdump "Problem mit nicht vertrauenswürdigen Eingaben in der PPP-Ausgabe behoben [CVE-2020-8037]">
<correction tigervnc "Zeritifkatsausnahmen im nativen und Java-VNC-Betrachter ordentlich speichern [CVE-2020-26117]">
<correction tor "Neue stabile Veröffentlichung der Originalautoren; multiple security, usability, portability, and reliability fixes">
<correction transmission "Speicherleck behoben">
<correction tzdata "Neue Veröffentlichung der Originalautoren">
<correction ublock-origin "Neue Version der Originalautoren; Plugin in Browser-spezifische Pakete aufgeteilt">
<correction vips "Verwendung nicht initialisierter Variablen unterbunden [CVE-2020-20739]">
</table>

<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2020 4766 rails>
<dsa 2020 4767 mediawiki>
<dsa 2020 4768 firefox-esr>
<dsa 2020 4769 xen>
<dsa 2020 4770 thunderbird>
<dsa 2020 4771 spice>
<dsa 2020 4772 httpcomponents-client>
<dsa 2020 4773 yaws>
<dsa 2020 4774 linux-latest>
<dsa 2020 4774 linux-signed-amd64>
<dsa 2020 4774 linux-signed-arm64>
<dsa 2020 4774 linux-signed-i386>
<dsa 2020 4774 linux>
<dsa 2020 4775 python-flask-cors>
<dsa 2020 4776 mariadb-10.3>
<dsa 2020 4777 freetype>
<dsa 2020 4778 firefox-esr>
<dsa 2020 4779 openjdk-11>
<dsa 2020 4780 thunderbird>
<dsa 2020 4781 blueman>
<dsa 2020 4782 openldap>
<dsa 2020 4783 sddm>
<dsa 2020 4784 wordpress>
<dsa 2020 4785 raptor2>
<dsa 2020 4786 libexif>
<dsa 2020 4787 moin>
<dsa 2020 4788 firefox-esr>
<dsa 2020 4789 codemirror-js>
<dsa 2020 4790 thunderbird>
<dsa 2020 4791 pacemaker>
<dsa 2020 4792 openldap>
<dsa 2020 4793 firefox-esr>
<dsa 2020 4794 mupdf>
<dsa 2020 4795 krb5>
<dsa 2020 4796 thunderbird>
<dsa 2020 4798 spip>
<dsa 2020 4799 x11vnc>
<dsa 2020 4800 libproxy>
</table>

<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction freshplayerplugin "Nicht mehr von den Browsern unterstützt; Entwicklung durch Originalautoren eingestellt">
<correction nostalgy "Inkompatibel mit neueren Thunderbird-Versionen">
<correction sieve-extension "Inkompatibel mit neueren Thunderbird-Versionen">
</table>

<h2>Debian-Installer</h2>

<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>


<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>

#use wml::debian::template title="Debian &ldquo;bullseye&rdquo;-utgivelsesinformasjon"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Hans F. Nordhaug <hansfn@gmail.com>

<if-stable-release release="bullseye">
<p>
  Debian <current_release_bullseye> ble utgitt <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>. 
  <ifneq "11.0" "<current_release>"
  "Debian 11.0 ble først utgitt <:=spokendate('XXXXXXXX'):>."
  />
  Utgivelsen inkluderte mange store endringer beskrevet i vår
  <a href="$(HOME)/News/XXXX/XXXXXXXX">pressemelding</a> og i
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

#<p><strong>Debian 11 has been superseded by
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, bullseye benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in bullseye.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>
  For å få tak og installere Debian, se siden med
  <a href="debian-installer/">installasjonsinformasjon</a> og 
  <a href="installmanual">installasjonshåndboken</a>. 
  For å oppgradere fra eldre Debian utgaver, se instruksjonene i 
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

### Activate the following when LTS period starts.
#<p>Følgende datamaskinarkitekturer er støttet i LTS-perioden:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Følgende datamaskinarkitekturer er støttet i denne utgaven:</p>
# <p>Da bullseye ble utgitt var følgende datamaskinarkitekturer støttet:</p> ### Use this line when LTS starts.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgaven selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problemer</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
</if-stable-release>

<if-stable-release release="buster">

<p>Kodenavnet på den neste store Debian-utgivelse etter 
<a href="../buster/">buster</a> er <q>bullseye</q>.</p>

<p>Denne utgaven begynte som en kopi av buster og er for øyeblikket i en fase
kalt <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>. 
Det betyr at ting ikke burde slutte å virke på en så alvorlig måte 
som i den ustabile eller eksperimentelle distribusjonen, fordi pakkene kun 
får lov til å bli med i distribusjonen etter et gitt tidsperiode har passert, 
og når der ikke er registrert utgivelseskritiske feil på dem.</p>

<p>Merk deg at sikkerhetsoppdateringer til <q>testing</q>-distribusjonen 
ennå <strong>ikke</strong> håndteres av sikkerhetsteamet. Derfor mottar 
<q>testing</q> <strong>ikke</strong> jevnlige sikkerhetsoppdateringer.  
Du oppfordres til å endre dine sources.list-linjer fra testing til buster inntil 
videre, hvis du har bruk for sikkerhetsbrukerstøtte. Se også punktet i 
<a href="$(HOME)/security/faq#testing">sikkerhetsteamets OSS</a> vedrørende
<q>testing</q>-distribusjonen.</p>

<p>Et <a href="releasenotes">utkast til utgivelsesmerknadene</a> kan være tilgjengelig. 
Sjekk også <a href="https://bugs.debian.org/release-notes">de foreslåtte
tilleggene til utgivelsesmerknadene</a>.</p>

<p>For installasjonsbilder og dokumentasjon om hvordan man installerer 
<q>testing</q>, se <a href="$(HOME)/devel/debian-installer/">siden om 
Debian-Installer</a>.</p>

<p>For å få flere opplysninger om hvordan <q>testing</q>-distribusjonen fungerer, se
<a href="$(HOME)/devel/testing">utviklernes opplysninger om den</a>.</p>

<p>Folk spør ofte om det fins en indikator for hvor langt vi er kommet med
utgaven. Dessverre fins det ikke en slik indikator, men vi kan vise til flere 
steder som beskriver ting som må behandles før utgivelsen kan skje:</p>

<ul>
    <li><a href="https://release.debian.org/">Generisk utgivelses-statusside</a></li>
    <li><a href="https://bugs.debian.org/release-critical/">Utgivelseskritiske feil</a></li>
    <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Feil i grunnsystemet</a></li>
    <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Feil i standard- og task-pakker</a></li>
</ul>

<p>I tillegg sender den utgivelsesansvarlige generelle statusrapporter på 
<a href="https://lists.debian.org/debian-devel-announce/">postlisten 
debian-devel-announce</a>.</p>

</if-stable-release>

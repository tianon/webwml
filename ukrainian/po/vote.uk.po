# translation of templates.po to Ukrainian
# Eugeniy Meshcheryakov <eugen@debian.org>, 2004, 2005, 2006.
# Volodymyr Bodenchuk <Bodenchuk@bigmir.net>, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: templates\n"
"PO-Revision-Date: 2017-11-15 23:40+0200\n"
"Last-Translator: Volodymyr Bodenchuk <Bodenchuk@bigmir.net>\n"
"Language-Team: Ukrainian <ukrainian>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Gtranslator 2.91.7\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n "
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Дата"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Часова шкала"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr "Резюме"

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Номінації"

#: ../../english/template/debian/votebar.wml:25
#, fuzzy
msgid "Withdrawals"
msgstr "Відхилене"

#: ../../english/template/debian/votebar.wml:28
msgid "Debate"
msgstr "Обговорення"

#: ../../english/template/debian/votebar.wml:31
msgid "Platforms"
msgstr "Платформи"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposer"
msgstr "Автор пропозиції"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal A Proposer"
msgstr "Автор пропозиції А"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal B Proposer"
msgstr "Автор пропозиції Б"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal C Proposer"
msgstr "Автор пропозиції В"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal D Proposer"
msgstr "Автор пропозиції Г"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal E Proposer"
msgstr "Автор пропозиції Д"

#: ../../english/template/debian/votebar.wml:52
msgid "Proposal F Proposer"
msgstr "Автор пропозиції Е"

#: ../../english/template/debian/votebar.wml:55
#, fuzzy
msgid "Proposal G Proposer"
msgstr "Автор пропозиції А"

#: ../../english/template/debian/votebar.wml:58
#, fuzzy
msgid "Proposal H Proposer"
msgstr "Автор пропозиції А"

#: ../../english/template/debian/votebar.wml:61
msgid "Seconds"
msgstr "Підтримують пропозицію"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal A Seconds"
msgstr "Підтримують пропозицію А"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal B Seconds"
msgstr "Підтримують пропозицію Б"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal C Seconds"
msgstr "Підтримують пропозицію В"

#: ../../english/template/debian/votebar.wml:73
msgid "Proposal D Seconds"
msgstr "Підтримують пропозицію Г"

#: ../../english/template/debian/votebar.wml:76
msgid "Proposal E Seconds"
msgstr "Підтримують пропозицію Д"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal F Seconds"
msgstr "Підтримують пропозицію Е"

#: ../../english/template/debian/votebar.wml:82
#, fuzzy
msgid "Proposal G Seconds"
msgstr "Підтримують пропозицію А"

#: ../../english/template/debian/votebar.wml:85
#, fuzzy
msgid "Proposal H Seconds"
msgstr "Підтримують пропозицію А"

#: ../../english/template/debian/votebar.wml:88
msgid "Opposition"
msgstr "Опозиція"

#: ../../english/template/debian/votebar.wml:91
msgid "Text"
msgstr "Текст"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal A"
msgstr "Пропозиція А"

#: ../../english/template/debian/votebar.wml:97
msgid "Proposal B"
msgstr "Пропозиція Б"

#: ../../english/template/debian/votebar.wml:100
msgid "Proposal C"
msgstr "Пропозиція В"

#: ../../english/template/debian/votebar.wml:103
msgid "Proposal D"
msgstr "Пропозиція Г"

#: ../../english/template/debian/votebar.wml:106
msgid "Proposal E"
msgstr "Пропозиція Д"

#: ../../english/template/debian/votebar.wml:109
msgid "Proposal F"
msgstr "Пропозиція Е"

#: ../../english/template/debian/votebar.wml:112
#, fuzzy
msgid "Proposal G"
msgstr "Пропозиція А"

#: ../../english/template/debian/votebar.wml:115
#, fuzzy
msgid "Proposal H"
msgstr "Пропозиція А"

#: ../../english/template/debian/votebar.wml:118
msgid "Choices"
msgstr "Варіанти вибору"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Proposer"
msgstr "Автор поправки"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Seconds"
msgstr "Підтримують поправку"

#: ../../english/template/debian/votebar.wml:127
msgid "Amendment Text"
msgstr "Текст поправки"

#: ../../english/template/debian/votebar.wml:130
msgid "Amendment Proposer A"
msgstr "Автор поправки А"

#: ../../english/template/debian/votebar.wml:133
msgid "Amendment Seconds A"
msgstr "Підтримують поправку А"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendment Text A"
msgstr "Текст поправки А"

#: ../../english/template/debian/votebar.wml:139
msgid "Amendment Proposer B"
msgstr "Автор поправки Б"

#: ../../english/template/debian/votebar.wml:142
msgid "Amendment Seconds B"
msgstr "Підтримують поправку Б"

#: ../../english/template/debian/votebar.wml:145
msgid "Amendment Text B"
msgstr "Текст поправки Б"

#: ../../english/template/debian/votebar.wml:148
msgid "Amendment Proposer C"
msgstr "Автор поправки В"

#: ../../english/template/debian/votebar.wml:151
msgid "Amendment Seconds C"
msgstr "Підтримують поправку В"

#: ../../english/template/debian/votebar.wml:154
msgid "Amendment Text C"
msgstr "Текст поправки В"

#: ../../english/template/debian/votebar.wml:157
msgid "Amendments"
msgstr "Поправки"

#: ../../english/template/debian/votebar.wml:160
msgid "Proceedings"
msgstr "Засідання"

#: ../../english/template/debian/votebar.wml:163
msgid "Majority Requirement"
msgstr "Умова більшості"

#: ../../english/template/debian/votebar.wml:166
msgid "Data and Statistics"
msgstr "Дані та статистика"

#: ../../english/template/debian/votebar.wml:169
msgid "Quorum"
msgstr "Кворум"

#: ../../english/template/debian/votebar.wml:172
msgid "Minimum Discussion"
msgstr "Мінімальне обговорення"

#: ../../english/template/debian/votebar.wml:175
msgid "Ballot"
msgstr "Бюлетень"

#: ../../english/template/debian/votebar.wml:178
msgid "Forum"
msgstr "Конференція"

#: ../../english/template/debian/votebar.wml:181
msgid "Outcome"
msgstr "Результат"

#: ../../english/template/debian/votebar.wml:185
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Очікує&nbsp;спонсорів"

#: ../../english/template/debian/votebar.wml:188
msgid "In&nbsp;Discussion"
msgstr "Розглядається"

#: ../../english/template/debian/votebar.wml:191
msgid "Voting&nbsp;Open"
msgstr "Голосування&nbsp;відкрите"

#: ../../english/template/debian/votebar.wml:194
msgid "Decided"
msgstr "Прийняте"

#: ../../english/template/debian/votebar.wml:197
msgid "Withdrawn"
msgstr "Відхилене"

#: ../../english/template/debian/votebar.wml:200
msgid "Other"
msgstr "Інше"

#: ../../english/template/debian/votebar.wml:204
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "На&nbsp;сторінку&nbsp;голосувань"

#: ../../english/template/debian/votebar.wml:207
msgid "How&nbsp;To"
msgstr "Як"

#: ../../english/template/debian/votebar.wml:210
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;пропозицію"

#: ../../english/template/debian/votebar.wml:213
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Внести&nbsp;поправку"

#: ../../english/template/debian/votebar.wml:216
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Підтримати&nbsp;пропозицію"

#: ../../english/template/debian/votebar.wml:219
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Взнати&nbsp;результат"

#: ../../english/template/debian/votebar.wml:222
msgid "Vote"
msgstr "Проголосувати"

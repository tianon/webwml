<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found by ClusterFuzz in leptonlib, an image
processing library.</p>

<p>All issues are related to heap-based buffer over-read in several functions
or a denial of service (application crash) with crafted data.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.74.1-1+deb9u1.</p>

<p>We recommend that you upgrade your leptonlib packages.</p>

<p>For the detailed security status of leptonlib please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/leptonlib">https://security-tracker.debian.org/tracker/leptonlib</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2612.data"
# $Id: $

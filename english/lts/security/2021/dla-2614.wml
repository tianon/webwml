<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The gunzip decompressor of Busybox, tiny utilities for small and embedded
systems, mishandled the error bit on the huft_build result pointer, with a
resultant invalid free or segmentation fault, via malformed gzip data.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.22.0-19+deb9u2.</p>

<p>We recommend that you upgrade your busybox packages.</p>

<p>For the detailed security status of busybox please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/busybox">https://security-tracker.debian.org/tracker/busybox</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2614.data"
# $Id: $

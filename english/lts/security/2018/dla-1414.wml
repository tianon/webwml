<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Some security vulnerabilities were found in Mercurial which allow
authenticated users to trigger arbitrary code execution and
unauthorized data access in certain server configuration. Malformed
patches and repositories can also lead to crashes and arbitrary code
execution on clients.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9462">CVE-2017-9462</a>

    <p>In Mercurial before 4.1.3, "hg serve --stdio" allows remote
    authenticated users to launch the Python debugger, and
    consequently execute arbitrary code, by using --debugger as a
    repository name.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-17458">CVE-2017-17458</a>

    <p>In Mercurial before 4.4.1, it is possible that a specially
    malformed repository can cause Git subrepositories to run
    arbitrary code in the form of a .git/hooks/post-update script
    checked into the repository. Typical use of Mercurial prevents
    construction of such repositories, but they can be created
    programmatically.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000132">CVE-2018-1000132</a>

    <p>Mercurial version 4.5 and earlier contains a Incorrect Access
    Control (CWE-285) vulnerability in Protocol server that can result
    in Unauthorized data access. This attack appear to be exploitable
    via network connectivity. This vulnerability appears to have been
    fixed in 4.5.1.</p>

<li>OVE-20180430-0001

    <p>mpatch: be more careful about parsing binary patch data</p></li>

<li>OVE-20180430-0002

    <p>mpatch: protect against underflow in mpatch_apply</p></li>

<li>OVE-20180430-0004

    <p>mpatch: ensure fragment start isn't past the end of orig</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.2-2+deb8u5.</p>

<p>We recommend that you upgrade your mercurial packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1414.data"
# $Id: $

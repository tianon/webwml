<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>The following vulnerabilities have been discovered in the Wheezy's
Wireshark version:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5350">CVE-2016-5350</a>

    <p>The SPOOLS dissector could go into an infinite loop</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5351">CVE-2016-5351</a>

    <p>The IEEE 802.11 dissector could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5353">CVE-2016-5353</a>

    <p>The UMTS FP dissector could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5354">CVE-2016-5354</a>

    <p>Some USB dissectors could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5355">CVE-2016-5355</a>

    <p>The Toshiba file parser could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5356">CVE-2016-5356</a>

    <p>The CoSine file parser could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5357">CVE-2016-5357</a>

    <p>The NetScreen file parser could crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5359">CVE-2016-5359</a>

    <p>The WBXML dissector could go into an infinite loop</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u6~deb7u2.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-538.data"
# $Id: $

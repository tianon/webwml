<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that Tor, a connection-based low-latency anonymous
communication system, may read one byte past a buffer when parsing
hidden service descriptors. This issue may enable a hostile hidden
service to crash Tor clients depending on hardening options and malloc
implementation.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.2.4.27-3.</p>

<p>We recommend that you upgrade your tor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-754.data"
# $Id: $

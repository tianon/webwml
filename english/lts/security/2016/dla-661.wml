<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Agostino Sarubbo of Gentoo discovered several security vulnerabilities
in libarchive, a multi-format archive and compression library. An
attacker could take advantage of these flaws to cause a buffer overflow
or an out of bounds read using a carefully crafted input file.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8687">CVE-2016-8687</a>

    <p>Agostino Sarubbo of Gentoo discovered a possible stack-based buffer
    overflow when printing a filename in bsdtar_expand_char() of util.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8688">CVE-2016-8688</a>

    <p>Agostino Sarubbo of Gentoo discovered a possible out of bounds read
    when parsing multiple long lines in bid_entry() and detect_form() of
    archive_read_support_format_mtree.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8689">CVE-2016-8689</a>

    <p>Agostino Sarubbo of Gentoo discovered a possible heap-based buffer
    overflow when reading corrupted 7z files in read_Header() of
    archive_read_support_format_7zip.c.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.0.4-3+wheezy5.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-661.data"
# $Id: $

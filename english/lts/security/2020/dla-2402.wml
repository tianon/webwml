<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11840">CVE-2019-11840</a>

    <p>An issue was discovered in supplementary Go cryptography libraries, aka
    golang-googlecode-go-crypto. If more than 256 GiB of keystream is
    generated, or if the counter otherwise grows greater than 32 bits, the amd64
    implementation will first generate incorrect output, and then cycle back to
    previously generated keystream. Repeated keystream bytes can lead to loss of
    confidentiality in encryption applications, or to predictability in CSPRNG
    applications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11841">CVE-2019-11841</a>

    <p>A message-forgery issue was discovered in
    crypto/openpgp/clearsign/clearsign.go in supplementary Go cryptography
    libraries. The <q>Hash</q> Armor Header specifies the message digest
    algorithm(s) used for the signature. Since the library skips Armor Header
    parsing in general, an attacker can not only embed arbitrary Armor Headers,
    but also prepend arbitrary text to cleartext messages without invalidating
    the signatures.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9283">CVE-2020-9283</a>

    <p>golang.org/x/crypto allows a panic during signature verification in the
    golang.org/x/crypto/ssh package. A client can attack an SSH server that accepts
    public keys. Also, a server can attack any SSH client.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:0.0~git20170407.0.55a552f+REALLY.0.0~git20161012.0.5f31782-1+deb8u1.</p>

<p>We recommend that you upgrade your golang-go.crypto packages.</p>

<p>For the detailed security status of golang-go.crypto please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-go.crypto">https://security-tracker.debian.org/tracker/golang-go.crypto</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2402.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in xymon, the network monitoring
application. Remote attackers might leverage these vulnerabilities in the CGI
parsing code (including buffer overflows and XSS) to cause denial of service,
or any other unspecified impact.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.3.17-6+deb8u2.</p>

<p>We recommend that you upgrade your xymon packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1898.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an authentication bypass
vulnerability in libxslt, a widely-used library for transforming
files from XML to other arbitrary format.</p>

<p>The xsltCheckRead and xsltCheckWrite routines permitted access upon
receiving an-1 error code and (as xsltCheckRead returned -1 for a
specially-crafted URL that is not actually invalid) the attacker was
subsequently authenticated.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11068">CVE-2019-11068</a>

    <p>libxslt through 1.1.33 allows bypass of a protection mechanism because
    callers of xsltCheckRead and xsltCheckWrite permit access even upon
    receiving a -1 error code. xsltCheckRead can return -1 for a crafted URL
    that is not actually invalid and is subsequently loaded.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.1.28-2+deb8u4.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1756.data"
# $Id: $

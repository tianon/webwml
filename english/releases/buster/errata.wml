#use wml::debian::template title="Debian 10 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Security issues</toc-add-entry>

<p>Debian security team issues updates to packages in the stable release
in which they've identified problems related to security. Please consult the
<a href="$(HOME)/security/">security pages</a> for information about
any security issues identified in <q>buster</q>.</p>

<p>If you use APT, add the following line to <tt>/etc/apt/sources.list</tt>
to be able to access the latest security updates:</p>

<pre>
  deb http://security.debian.org/ buster/updates main contrib non-free
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Point releases</toc-add-entry>

<p>Sometimes, in the case of several critical problems or security updates, the
released distribution is updated.  Generally, these are indicated as point
releases.</p>

<ul>
  <li>The first point release, 10.1, was released on
      <a href="$(HOME)/News/2019/20190907">September 7, 2019</a>.</li>
  <li>The second point release, 10.2, was released on
      <a href="$(HOME)/News/2019/20191116">November 16, 2019</a>.</li>
 <li>The third point release, 10.3, was released on
      <a href="$(HOME)/News/2020/20200208">February 8, 2020</a>.</li>
 <li>The fourth point release, 10.4, was released on
      <a href="$(HOME)/News/2020/20200509">May 9, 2020</a>.</li>
 <li>The fifth point release, 10.5, was released on
      <a href="$(HOME)/News/2020/20200801">August 1, 2020</a>.</li>
 <li>The sixth point release, 10.6, was released on
      <a href="$(HOME)/News/2020/20200926">September 26, 2020</a>.</li>
 <li>The seventh point release, 10.7, was released on
      <a href="$(HOME)/News/2020/20201205">December 5, 2020</a>.</li>
 <li>The eighth point release, 10.8, was released on
      <a href="$(HOME)/News/2021/20210206">February 6, 2021</a>.</li>
 <li>The ninth point release, 10.9, was released on
      <a href="$(HOME)/News/2021/20210327">March 27, 2021</a>.</li>
</ul>

<ifeq <current_release_buster> 10.0 "

<p>There are no point releases for Debian 10 yet.</p>" "

<p>See the <a
href="http://http.us.debian.org/debian/dists/buster/ChangeLog">\
ChangeLog</a>
for details on changes between 10 and <current_release_buster/>.</p>"/>


<p>Fixes to the released stable distribution often go through an
extended testing period before they are accepted into the archive.
However, these fixes are available in the
<a href="http://ftp.debian.org/debian/dists/buster-proposed-updates/">\
dists/buster-proposed-updates</a> directory of any Debian archive
mirror.</p>

<p>If you use APT to update your packages, you can install
the proposed updates by adding the following line to
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 10 point release
  deb http://deb.debian.org/debian buster-proposed-updates main contrib non-free
</pre>

<p>After that, run <kbd>apt update</kbd> followed by
<kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Installation system</toc-add-entry>

<p>
For information about errata and updates for the installation system, see
the <a href="debian-installer/">installation information</a> page.
</p>

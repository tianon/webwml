<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20503">CVE-2019-20503</a>

   <p>Natalie Silvanovich discovered an out-of-bounds read issue in the usrsctp
   library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6422">CVE-2020-6422</a>

    <p>David Manouchehri discovered a use-after-free issue in the WebGL
    implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6424">CVE-2020-6424</a>

    <p>Sergei Glazunov discovered a use-after-free issue.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6425">CVE-2020-6425</a>

    <p>Sergei Glazunov discovered a policy enforcement error related to
    extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6426">CVE-2020-6426</a>

    <p>Avihay Cohen discovered an implementation error in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6427">CVE-2020-6427</a>

    <p>Man Yue Mo discovered a use-after-free issue in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6428">CVE-2020-6428</a>

    <p>Man Yue Mo discovered a use-after-free issue in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6429">CVE-2020-6429</a>

    <p>Man Yue Mo discovered a use-after-free issue in the audio implementation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6449">CVE-2020-6449</a>

    <p>Man Yue Mo discovered a use-after-free issue in the audio implementation.</p></li>

</ul>

<p>For the oldstable distribution (stretch), security support for chromium has
been discontinued.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 80.0.3987.149-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4645.data"
# $Id: $

#use wml::debian::cdimage title="Live installatie-images"
#use wml::debian::release_info
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="8c86ac02236495359e82eed0e3b9e29905514fd7"

<p>Een <q>live installatie</q>-image bevat een Debian-systeem dat kan
opstarten zonder een bestand op de harde schijf te wijzigen. Ook maakt de
inhoud van het image de installatie van Debian mogelijk.
</p>

<p><a name="choose_live"><strong>Is een live-image geschikt voor mij?</strong></a>
Hierna volgen een aantal in aanmerking te nemen zaken die u kunnen helpen een beslissing te nemen.
<ul>
<li><b>Varianten:</b> De live-images bestaan in verschillende "varianten"
die een keuze bieden uit bureaubladomgevingen (GNOME, KDE, LXDE, Xfce,
Cinnamon en MATE). Veel gebruikers zullen de initiële selectie pakketten
geschikt vinden en nadien eventuele bijkomende pakketten welke ze nodig hebben,
via het netwerk installeren.
<li><b>Architectuur:</b> Momenteel worden enkel images voor de twee
populairste architecturen, 32-bits PC (i386) en 64-bits PC (amd64),
aangeboden.
<li><b>Installatieprogramma:</b> Vanaf Debian 10 Buster bevatten de live-images
het gebruikersvriendelijke <a href="https://calamares.io">Calamares
installatieprogramma</a>, een distributie-onafhankelijk installatieprogramma,
als een alternatief voor ons welbekend
<a href="$(HOME)/devel/debian-installer">Debian-installatiesysteem</a>.
<li><b>Grootte:</b> Elk image is veel kleiner dan de volledige collectie op
dvd-images, maar groter dan het netwerkinstallatiemedium.
<li><b>Taal:</b> De images bevatten niet de volledige verzameling pakketten
voor taalondersteuning. Als u invoermethoden, lettertypes of bijkomende
pakketten nodig heeft voor uw taal, moet u deze nadien installeren.
</ul>

<p>U kunt de volgende live installatie-images downloaden:</p>

<ul>

  <li>Officiële <q>live installatie</q>-images voor de <q>stabiele</q> release &mdash; <a
  href="#live-install-stable">zie hieronder</a></li>

</ul>


<h2 id="live-install-stable">Officiële live installatie-images voor de <q>stabiele</q> release</h2>

<p>Aangeboden in verschillende varianten, die, zoals hierboven aangegeven,
onderling in grootte verschillen. Deze images zijn geschikt om een
Debian-systeem met een geselecteerde collectie standaardpakketten uit te
proberen en ze met behulp van diezelfde media te installeren.</p>

<div class="line">
<div class="item col50">
<p><strong>dvd/USB (via <a
href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een dvd-r(w) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.
Indien u dat kunt, gebruik dan BitTorrent, omdat dit de belasting van
onze servers beperkt.</p>
	  <stable-live-install-bt-cd-images />
</div>

<div class="item col50 lastcol"> <p><strong>dvd/USB</strong></p>
<p><q>Hybride</q> ISO imagebestanden die op een dvd-r(w) gebrand kunnen
worden en ook naar een voldoende grote USB-stick gekopieerd kunnen worden.</p>
       <stable-live-install-iso-cd-images />
</div> </div>

<p>Raadpleeg de <a href="../faq/">FAQ</a> voor informatie over wat deze
bestanden zijn en hoe u ze kunt gebruiken.</p>

<p>Indien u van plan bent Debian te installeren met het gedownloade live-image,
moet u zeker de <a href="$(HOME)/releases/stable/installmanual">uitgebreide
informatie over het installatieproces</a> bekijken.</p>

<p>Raadpleeg <a href="$(HOME)/devel/debian-live"> de projectpagina van Debian Live</a> voor
extra informatie over de Debian Live-systemen die op deze images aangeboden worden.</p>

# Translators: the following paragraph exists (in this or a similar form) several times in webwml,
# so please try to keep translations consistent. See:
# ./CD/http-ftp/index.wml
# ./CD/live/index.wml
# ./CD/netinst/index.wml
# ./CD/torrent-cd/index.wml
# ./distrib/index.wml
# ./distrib/netinst.wml
# ./releases/<release-codename>/debian-installer/index.wml
# ./devel/debian-installer/index.wml
#
<h2><a name="firmware">Niet-officiële cd/dvd-images die niet-vrije firmware bevatten</a></h2>

<div id="firmware_nonfree" class="important">
<p>
Als op uw systeem bepaalde hardware <strong>vereist dat niet-vrije firmware
geladen wordt</strong> samen met het stuurprogramma voor het apparaat, kunt u
een van de <a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stable/current/">tar-archieven met gangbare firmware-pakketten</a>
gebruiken of een <strong>niet-officieel</strong> image downloaden dat deze
<strong>niet-vrije</strong> firmware bevat. Instructies over het gebruik van
tar-archieven en algemene informatie over het laden van firmware tijdens een
installatie kunt u vinden in de
<a href="../../releases/stable/amd64/ch06s04">Installatiehandleiding</a>.
</p>
<p>
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/current-live/">niet-officiële live-images voor de release
<q>stable</q> die firmware bevatten</a>
</p>
</div>




#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans la bibliothèque libtiff et
les outils inclus qui pourrait aboutir à un déni de service:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-11613">CVE-2017-11613</a>

<p>Vulnérabilité de déni de service dans la fonction TIFFOpen. Une entrée
contrefaite peut conduire à une attaque par déni de service et peut aussi
planter le système ou déclencher le tueur OOM.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5784">CVE-2018-5784</a>

<p>Consommation de ressources incontrôlée dans la fonction TIFFSetDirectory de
src/libtiff/tif_dir.c, qui peut causer un déni de service à l’aide d’un fichier
tif contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u21.</p>
<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1391.data"
# $Id: $

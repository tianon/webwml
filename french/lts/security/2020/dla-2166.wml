#use wml::debian::translation-check translation="d809e8a47b5e419f7e1225cd2d4ed884d4cb964f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le module PAM de krb5 (pam_krb5.so) contenait un dépassement de tampon
pouvant provoquer une exécution de code à distance dans des situations impliquant
des invites supplémentaires par une bibliothèque Kerberos. Cela pouvait
dépasser la limite du tampon fourni par la bibliothèque Kerberos sous-jacente
d’un seul octet « \0 » si un attaquant réagissait à une invite avec une réponse
d’une longueur soigneusement choisie. L’effet pouvait se situer entre une
corruption de pile et une corruption de tas selon la structure de la bibliothèque
Kerberos sous-jacente, avec des effets inconnus mais possiblement une exécution
de code. Cette méthode de code n’était pas utilisée pour une authentification
normale, mais seulement lorsque la bibliothèque Kerberos réalisait des invites
supplémentaires, comme avec PKINIT ou lors de l’utilisation de l’option
no_prompt non standard de PAM.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 4.6-3+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libpam-krb5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2166.data"
# $Id: $

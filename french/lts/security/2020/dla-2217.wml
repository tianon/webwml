#use wml::debian::translation-check translation="7882b5b7ec70cb22d1377e56ab2123a55e821785" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une exécution potentielle de code
à distance à l’aide d’une désérialisation dans tomcat7, un serveur pour HTTP et
les « servlets » Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

<p>Lors de l’utilisation d’Apache Tomcat, versions 10.0.0-M1 à 10.0.0-M4,
9.0.0.M1 à 9.0.34, 8.5.0 à 8.5.54 et 7.0.0 à 7.0.103, si : a) un attaquant était
capable de contrôler le contenu et le nom d’un fichier sur le serveur, b)
le serveur était configuré pour utiliser PersistenceManager avec un FileStore,
c) le PersistenceManager était configuré avec
sessionAttributeValueClassNameFilter="null" (la valeur par défaut à moins qu’un
SecurityManager soit utilisé) ou qu’un filtre assez faible permettait la
désérialisation de l’objet fourni par l’attaquant, d) l’attaquant connaissait le
chemin relatif dans l'emplacement de stockage utilisé par FileStore
du fichier dont il avait le contrôle, alors, en utilisant une requête
spécialement contrefaite, l’attaquant était capable de déclencher une exécution
de code à distance par la désérialisation d’un fichier sous son contrôle.
Il est à remarquer que toutes les conditions (a à d) devaient être satisfaites
pour la réussite de l’attaque.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 7.0.56-3+really7.0.100-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2217.data"
# $Id: $

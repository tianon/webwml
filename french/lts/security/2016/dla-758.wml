#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une couleur non valable provoque un épuisement de pile par un appel
récursif à la fonction gdImageFillToBorder quand l'image utilisée est
truecolor.</p>

<p>La vulnérabilité peut être exploitée à travers php5 qui utilise la
bibliothèque libgd2 du système au moyen de la fonction imagefilltoborder()
de PHP pour provoquer un déni de service (dépassement de pile) et
éventuellement avoir un autre impact non précisé.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.0.36~rc1~dfsg-6.1+deb7u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgd2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-758.data"
# $Id: $

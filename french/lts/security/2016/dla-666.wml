#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans GNU Guile, une
implémentation du langage de programmation Scheme. Le projet « Common
Vulnerabilities and Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8605">CVE-2016-8605</a>

<p>La procédure mkdir de GNU Guile modifiait temporairement l'umask du
processus à zéro. Pendant ce laps de temps, dans une application
multiprocessus, les autres processus pourraient finir par créer des
fichiers avec des permissions non sûres.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-8606">CVE-2016-8606</a>

<p>GNU Guile fournit un <q>serveur REPL</q> qui est une invite
d'interpréteur de commande auquel les développeurs peuvent se connecter
pour de la programmation à la volée et aux fins de débogage. Le serveur
REPL est démarré avec l'option de ligne de commande « --listen » ou une API
équivalente.</p>

<p>Il a été signalé que le serveur REPL est vulnérable l'attaque
inter-protocole HTTP.</p>

<p>Cela constitue une vulnérabilité d'exécution distante de code pour des
développeurs faisant fonctionner un serveur REPL qui écoute un périphérique
« loopback » ou un réseau privé. Les applications qui n'exécutent pas un
serveur REPL, comme c'est le cas habituellement, ne sont pas affectées.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.0.5+1-3+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets guile-2.0.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-666.data"
# $Id: $

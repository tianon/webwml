#use wml::debian::translation-check translation="c8457a59a89d51637f9c29eabc2b64c8a52130b6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Le programme tcprewrite, un élément de la suite tcpreplay, ne vérifie
pas la taille des trames qu'il traite. Des trames très grandes peuvent
déclencher une erreur de segmentation, et ce type de trames survient
lors de la capture de paquets sur des interfaces avec un MTU de ou proche
de 65536. Par exemple, l'interface de boucle locale lo du noyau Linux a une
valeur de ce type.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 3.4.3-2+wheezy2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tcpreplay.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-544.data"
# $Id: $

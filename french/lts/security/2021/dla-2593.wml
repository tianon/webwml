#use wml::debian::translation-check translation="02a35e572daca538cb30585aa9321eb5b1b05ad5" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour rétablit la liste noire de Symantec CA (originellement du bogue
n° 911289). Les certificats racine suivants ont été rajoutés (+) :
</p><ul>
<li>+ <q>GeoTrust Global CA</q></li>
<li>+ <q>GeoTrust Primary Certification Authority</q></li>
<li>+ <q>GeoTrust Primary Certification Authority - G2</q></li>
<li>+ <q>GeoTrust Primary Certification Authority - G3</q></li>
<li>+ <q>GeoTrust Universal CA</q></li>
<li>+ <q>thawte Primary Root CA</q></li>
<li>+ <q>thawte Primary Root CA – G2</q></li>
<li>+ <q>thawte Primary Root CA – G3</q></li>
<li>+ <q>VeriSign Class 3 Public Primary Certification Authority – G4</q></li>
<li>+ <q>VeriSign Class 3 Public Primary Certification Authority – G5</q></li>
<li>+ <q>VeriSign Universal Root Certification Authority</q></li>
</ul>
<p><b>Remarque</b> : à cause du bogue n° 743339, les certificats CA rajoutés
dans cette version ne seront pas automatiquement acceptés de nouveau lors
d’une mise à niveau. Les utilisateurs affectés devront peut être reconfigurer
le paquet pour retrouver l’état désiré.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 20200601~deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ca-certificates.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ca-certificates, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ca-certificates">\
https://security-tracker.debian.org/tracker/ca-certificates</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2593.data"
# $Id: $

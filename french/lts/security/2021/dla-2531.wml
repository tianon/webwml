#use wml::debian::translation-check translation="4f3fc86f3b107718b6c33026841f79d68ce588ba" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les paquets src:python-bottle avant la version 0.12.19 sont vulnérables à un
empoisonnement de cache web en utilisant un masquage de paramètre d’appel de
vecteur.</p>

<p>Lorsque l’attaquant peut séparer les paramètres de requête en utilisant un
point-virgule, il peut provoquer une différence dans l’interprétation de la
requête par le mandataire (utilisé avec la configuration par défaut) et le
serveur. Cela peut aboutir à des requêtes malveillantes mises en cache de la
même manière que celles de bonne facture parce que le mandataire 
ne considère habituellement pas le point-virgule comme un séparateur et, par conséquent, ne
l’inclut pas dans une clé de cache d’un paramètre sans clé.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.12.13-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-bottle.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-bottle, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-bottle">https://security-tracker.debian.org/tracker/python-bottle</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2531.data"
# $Id: $

#use wml::debian::translation-check translation="bedd4683f14a5bb13d9f24e0ebb58423ce0059f3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>cloud-init possède la capacité de générer et définir des mots de passe
aléatoires pour les utilisateurs du système. Cela est réalisé lors de
l’exécution en passant des données cloud-config telles que :</p>

<p>chpasswd:
 list: |
utilisateur1:RANDOM</p>

<p>Lorsqu’utilisé de cette façon, cloud-init journalise le mot de passe brut et
non chiffré dans un fichier local lisible par tous.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.7.9-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cloud-init.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cloud-init, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cloud-init">\
https://security-tracker.debian.org/tracker/cloud-init</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2601.data"
# $Id: $

#use wml::debian::translation-check translation="725b6dd711e411f96401c313b37918a6f67775bb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a> 
<a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-28660">CVE-2021-28660</a>
Debian Bug: 983595</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à l'exécution de code arbitraire, une élévation des
privilèges, un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27170">CVE-2020-27170</a>

<p>, <a href="https://security-tracker.debian.org/tracker/CVE-2020-27171">CVE-2020-27171</a>.</p>

<p>Piotr Krysiuk a découvert des défauts dans les vérifications du sous-système
BPF pour une fuite d'informations à travers une exécution spéculative. Un
utilisateur local pourrait utiliser cela pour obtenir des informations sensibles
de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3348">CVE-2021-3348</a>

<p>ADlab de venustech a découvert une situation de compétition dans le pilote
de bloc nbd qui peut conduire à une utilisation de mémoire après libération. Un
utilisateur local avec accès au périphérique bloc nbd pourrait utiliser cela
pour provoquer un déni de service (plantage ou corruption de mémoire) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3428">CVE-2021-3428</a>

<p>Wolfgang Frisch a signalé un dépassement potentiel d'entier dans le pilote de
système de fichiers ext4. Un utilisateur autorisé à monter une image arbitraire
de système de fichiers pourrait utiliser cela pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26930">CVE-2021-26930</a>

<p>(XSA-365)</p>

<p>Olivier Benjamin, Norbert Manthey, Martin Mazein et Jan H. Schönherr ont
découvert que le pilote de dorsal de bloc Xen (xen-blkback) ne gérait pas
correctement les erreurs de mappage d’allocation. Un invité malveillant pourrait
exploiter ce bogue pour provoquer un déni de service (plantage), ou éventuellement
une fuite d'informations ou une élévation des privilèges, dans le domaine
exécutant le dorsal, qui est classiquement dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26931">CVE-2021-26931</a>

<p>(XSA-362), <a href="https://security-tracker.debian.org/tracker/CVE-2021-26932">CVE-2021-26932</a> (XSA-361), <a href="https://security-tracker.debian.org/tracker/CVE-2021-28038">CVE-2021-28038</a> (XSA-367)</p>

<p>Jan Beulich a découvert que le code de prise en charge de Xen et de divers
pilotes de dorsal Xen ne gérait pas correctement les erreurs de mappage
d’allocation. Un invité malveillant pourrait exploiter ce bogue pour provoquer
un déni de service (plantage), ou éventuellement une fuite d'informations ou une
élévation des privilèges, dans le domaine exécutant le dorsal, qui est
classiquement dom0.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27363">CVE-2021-27363</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement l’accès aux attributs de gestion de transport dans sysfs. Dans un
système agissant comme initiateur iSCSI, cela constitue une fuite d'informations
vers des utilisateurs locaux et facilite l’exploitation du <a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27364">CVE-2021-27364</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement l’accès à son interface de gestion netlink. Dans un système
agissant comme initiateur iSCSI, un utilisateur local pourrait utiliser cela
pour provoquer un déni de service (déconnexion du stockage) ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-27365">CVE-2021-27365</a>

<p>Adam Nichols a signalé que le sous-système initiateur iSCSI ne restreignait pas
correctement la longueur des paramètres ou des <q>passthrough PDU</q> envoyés
à travers son interface de gestion netlink. Dans un système agissant comme
initiateur iSCSI, un utilisateur local pourrait utiliser cela pour faire fuiter
le contenu de la mémoire du noyau, pour provoquer un déni de service (corruption
de mémoire du noyau ou plantage) et probablement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28660">CVE-2021-28660</a>

<p>Il a été découvert que le pilote Wi-Fi rtl8188eu ne limitait pas correctement
la longueur des SSID copiés dans les résultats d’analyse. Un attaquant dans la
portée du Wi-Fi pourrait utiliser cela pour provoquer un déni de service
(plantage ou corruption de mémoire) ou, éventuellement, exécuter du code sur le
système vulnérable.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.181-1~deb9u1. De plus, cette mise à jour corrige le bogue
n° 983595 et inclut plusieurs autres corrections de bogue à partir de mises à
jour de la version stable 4.19.172–4.19.181 (inclus).</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">\
https://security-tracker.debian.org/tracker/linux-4.19</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2610.data"
# $Id: $

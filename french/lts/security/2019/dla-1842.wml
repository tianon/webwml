#use wml::debian::translation-check translation="16096af1322d54c0f85a9fb34599fdbf0a729002" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Django, un cadre de développement web en Python, n’identifiait pas correctement
les connexions HTTP quand un mandataire inverse se connectait à l’aide d’HTTPS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12308">CVE-2019-12781 :
détection HTTP incorrecte avec mandataire inverse se connectant à l’aide d’HTTPS</a>

<p>Lorsque déployé derrière un mandataire inverse se connectant à Django à l’aide
d’HTTPS, <tt>django.http.HttpRequest.scheme</tt> détecte incorrectement les
requêtes de client faites à l’aide d’HTTP comme utilisant HTTPS. Cela engendre des
résultats incorrects pour <tt>is_secure()</tt> et <tt>build_absolute_uri()</tt>,
et cette requête HTTP ne sera pas redirigée vers HTTPS conformément
à <tt>SECURE_SSL_REDIRECT</tt>.</p>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.7.11-1+deb8u6.</p>
<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1842.data"
# $Id: $

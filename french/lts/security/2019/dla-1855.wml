#use wml::debian::translation-check translation="9623afbea568887444d6d1017f69074761395049" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il existait une vulnérabilité de dépassement d’entier dans exiv2, un outil
pour manipuler des images contenant (par exemple) des métadonnées EXIF.</p>

<p>Cela pouvait aboutir à un déni de service à l'aide d'un fichier contrefait
pour l’occasion.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13504">CVE-2019-13504</a>

<p>Lecture hors limites dans Exiv2::MrwImage::readMetadata dans mrwimage.cpp
dans Exiv2 jusqu’à 0.27.2.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.24-4.1+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1855.data"
# $Id: $

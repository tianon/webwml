#use wml::debian::translation-check translation="6259ee5c5e06156d1325f026568cbf234eefb7e8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Juraj Somorovsky, Robert Merget et Nimrod Aviram ont découvert une attaque
d’oracle par remplissage dans OpenSSL.</p>

<p>Si une application rencontrait une erreur fatale de protocole et appelait
alors SSL_shutdown() deux fois (une fois pour envoyer un close_notify et une
autre pour en recevoir un), alors OpenSSL pouvait répondre différemment
à l’application appelante si un enregistrement de 0 octet est reçu avec un
remplissage non valable comparé à un enregistrement de 0 octet reçu avec un MAC non
valable. Si l’application alors se comporte différemment, basé sur que de cette
manière cela est détectable par le pair distant, alors cela équivaut à l’oracle
de remplissage qui pourrait être utilisé pour déchiffrer les données.</p>

<p>Pour que cela soit exploitable, les suites de chiffrement « non-orchestrées »
doivent être utilisées. Les suites de chiffrement orchestrées (Stitched) sont
des implémentations optimisées de certaines suites couramment utilisées.
L’application doit aussi appeler deux fois SSL_shutdown() même si une erreur
de protocole s’est produite (les applications ne devraient pas faire cela, mais
quelques unes le font). Les suites AEAD ne sont pas touchées.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 1.0.1t-1+deb8u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openssl.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1701.data"
# $Id: $

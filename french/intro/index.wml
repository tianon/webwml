#use wml::debian::template title="Introduction à Debian"
#use wml::debian::recent_list

#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c" maintainer="Jean-Pierre Giraud"

<a id=community></a>
<h2>Debian est une communauté de personnes</h2>
<p>Des milliers de volontaires à travers le monde travaillent ensemble en
  priorisant le logiciel libre et les besoins des utilisateurs.</p>


<ul>
  <li>
    <a href="../intro/about">Les acteurs :</a>
    qui sommes nous, que faisons nous ?
  </li>
  <li>
    <a href="../intro/">Philosophie :</a>
    pourquoi et comment nous le faisons
  </li>
  <li>
    <a href="../devel/join/">S'impliquer :</a>
    vous pouvez faire partie de la communauté !
  </li>
  <li>
    <a href="help">Comment pouvez-vous aider Debian ?</a>
  </li>
  <li>
    <a href="../social_contract">Le contrat social :</a>
    nos intentions morales
  </li>
  <li>
    <a href="diversity">Diversité et équité</a>
  </li>
  <li>
    <a href="../code_of_conduct">Code de conduite</a>
  </li>
  <li>
    <a href="../partners/">Partenariat :</a>
    entreprises et organisations qui fournissent une assistance permanente
    au projet Debian
  </li>
  <li>
    <a href="../donations">Dons</a>
  </li>
  <li>
    <a href="../legal/">Informations légales</a>
  </li>
  <li>
    <a href="../legal/privacy">Protection des données</a>
  </li>
  <li>
    <a href="../contact">Nous contacter</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian est un système d'exploitation libre</h2>
<p>Nous nous basons sur le noyau Linux auquel nous avons ajouté plusieurs
milliers d'applications pour répondre aux besoins des utilisateurs.</p>

<ul>
  <li>
    <a href="../distrib">Téléchargement :</a>
    plus de versions d'image de Debian
  </li>
  <li>
  <a href="why_debian">Pourquoi Debian ?</a>
  </li>
  <li>
    <a href="../support">Assistance :</a>
    obtenir de l'aide
  </li>
  <li>
    <a href="../security">Sécurité :</a>
    dernières mises à jour
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Paquets logiciels :</a>
    rechercher et naviguer dans la longue liste de nos logiciels
  </li>
  <li>
    <a href="../doc">Documentation</a>
  </li>
  <li>
    <a href="https://wiki.debian.org">Wiki Debian</a>
  </li>
  <li>
    <a href="../Bugs">Rapport de bogues</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Listes de diffusion</a>
  </li>
  <li>
    <a href="../blends">Pure Blends :</a>
    métapaquets pour des besoins spécifiques
  </li>
  <li>
    <a href="../devel">Le coin du développeur :</a>
    informations intéressant principalement les développeurs Debian
  </li>
  <li>
    <a href="../ports">Portages et architectures :</a>
    architectures de processeur prises en charge
  </li>
  <li>
    <a href="search">Comment utiliser le moteur de recherche de Debian ?</a>
  </li>
  <li>
    <a href="cn">Pages disponibles dans différentes langues</a>.
  </li>
</ul>

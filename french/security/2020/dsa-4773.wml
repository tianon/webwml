#use wml::debian::translation-check translation="7c87d7393452c5b5f751f8802a32709ff0440f1d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans yaws, un serveur web
HTTP 1.1 très performant écrit en Erlang.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24379">CVE-2020-24379</a>

<p>L'implémentation de WebDAV est prédisposée à une vulnérabilité
d'injection d'entités externes XML (XXE).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24916">CVE-2020-24916</a>

<p>L'implémentation de CGI ne nettoyait pas correctement les requêtes CGI
permettant à un attaquant distant d'exécuter des commandes d'interpréteur
arbitraires au moyen de noms d'exécutables CGI contrefaits pour l'occasion.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.0.6+dfsg-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets yaws.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de yaws, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/yaws">\
https://security-tracker.debian.org/tracker/yaws</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4773.data"
# $Id: $

#use wml::debian::template title="Informazioni sul rilascio di Debian &ldquo;bullseye&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="c8265f664931f5ce551446ba751d63d63b70ce86" maintainer="Luca Monducci"

<if-stable-release release="bullseye">

<p>Debian GNU/Linux <current_release_bullseye> è stata rilasciata il
<a href="$(HOME)/News/<current_release_newsurl_bullseye/>">
<current_release_date_bullseye></a>.
<ifneq "11.0" "<current_release>"
	"Il rilascio iniziale di Debian 11.0 fu fatto il <:=spokendate('XXXXXXXX'):>."
/>
Questo rilascio contiene importanti cambiamenti descritti
nel <a href="$(HOME)/News/XXXX/XXXXXXXX">comunicato stampa</a> e
nelle <a href="releasenotes">Note di rilascio</a>.</p>

#<p><strong>Debian 11 è stata sostituita da
#<a href="../bookworm/">Debian 12 (<q>bookworm</q>)</a>.
#Gli aggiornamenti per la sicurezza sono stati interrotti 
#dal <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Nonostante bullseye benefici anche del Supporto a Lungo Termine
#(LTS Long Term Support) fino a xxxx 20xx. Tale supporto è limitato alle
#architetture i386, amd64, armel, armhf e arm64; tutte le altre architetture
#non hanno supporto. Per ulteriori informazioni fare riferimento alla <a
#href="https://wiki.debian.org/LTS">sezione LTS del Wiki Debian</a>.
#</strong></p>

<p>Per ottenere e installare Debian, si veda la pagina
con le <a href="debian-installer/">informazioni sull'installazione</a> e
la <a href="installmanual">Guida all'installazione</a>. Per aggiornare
da un precedente rilascio di Debian, consultare le
<a href="releasenotes">Note di rilascio</a>.</p>

### Activate the following when LTS period starts.
#<p>Architetture gestite nel periodo Long Term Support:</p>
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Architetture supportate al momento del rilascio iniziale di bullseye:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Nonstante la nostra volontà, questo rilascio potrebbe avere problemi,
anche se è chiamato <em>stable</em>. Esiste un <a href="errata">elenco
dei principali problemi conosciuti</a>, ed è possibile <a
href="reportingbugs">segnalare altri problemi</a>.</p>

<p>Infine, ma non meno importante, è presente un elenco di
<a href="credits">persone da ringraziare</a> per aver permesso questo
rilascio.</p>
</if-stable-release>

<if-stable-release release="buster">

<p>Il nome in codice della prossima versione principale di Debian dopo
<a href="../buster/">buster</a> è <q>bullseye</q>.</p>

<p>Questo rilascio è partito come una copia di buster ed è attualmente in
uno stato chiamato <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>.
Questo vuol dire che non dovrebbe avere problemi fatali come quelli che
potrebbero essere nelle distribuzioni unstable o experimental, poiché i
pacchetti possono entrare nella distribuzione testing solo dopo un certo
periodo di tempo e solo se non hanno alcun bug di gravità critica per il
rilascio aperto.</p>

<p>Da notare che gli aggiornamenti della sicurezza per la distribuzione
<q>testing</q> <strong>non</strong> sono gestiti dal team della sicurezza.
Per questo motivo <q>testing</q> <strong>non</strong> dispone degli
aggiornamenti per la sicurezza in tempi stretti.
# Per maggiori informazioni si veda
# l'<a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">\
# avviso</a> del Testing Security Team.
Si raccomanda di modificare il file
sources.list da testing a buster per il periodo di tempo in cui si ha
bisogno del supporto per la sicurezza. Si veda la voce relativa alla
distribuzione <q>testing</q> nelle <a href="$(HOME)/security/faq#testing">FAQ
del Security Team</a>.</p>

<p>È disponibile una <a href="releasenotes">bozza delle note di
rilascio</a>; consultare anche le
<a href="https://bugs.debian.org/release-notes">proposte di aggiunta alle
note di rilascio</a>.</p>

<p>Per ulteriori informazioni su immagini e documentazione per
l'installazione di <q>testing</q>, si consulti la
<a href="$(HOME)/devel/debian-installer/">pagina dell'Installatore
Debian (Debian-Installer)</a>.</p>

<p>Per approfondire il funzionamento della distribuzione <q>testing</q>
si vedano <a href="$(HOME)/devel/testing">le informazioni per lo
sviluppatore</a>.</p>

<p>Spesso viene chiesto se esiste un semplice <q>indicatore dello
stato di avanzamento</q>. Sfortunatamente tale strumento non esiste, ma
possiamo indicare dove controllare i passi ancora da fare
prima che avvenga il rilascio:</p>

<ul>
  <li><a href="https://release.debian.org/">Stato generale del rilascio</a></li>
  <li><a href="https://bugs.debian.org/release-critical/">Bug di gravità critica per il rilascio</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Bug nel sistema di base</a></li>
  <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Bug nei pacchetti standard e task</a></li>
</ul>

<p>Inoltre, rapporti sullo stato generale sono inviati dal manager del
rilascio alla lista di messaggi
<a href="https://lists.debian.org/debian-devel-announce/">\
debian-devel-announce</a>.</p>

</if-stable-release>

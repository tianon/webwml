msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-04-26 14:27-0400\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/devel/join/nm-steps.inc:7
msgid "New Members Corner"
msgstr "新成员专区"

#: ../../english/devel/join/nm-steps.inc:10
msgid "Step 1"
msgstr "第一步"

#: ../../english/devel/join/nm-steps.inc:11
msgid "Step 2"
msgstr "第二步"

#: ../../english/devel/join/nm-steps.inc:12
msgid "Step 3"
msgstr "第三步"

#: ../../english/devel/join/nm-steps.inc:13
msgid "Step 4"
msgstr "第四步"

#: ../../english/devel/join/nm-steps.inc:14
msgid "Step 5"
msgstr "第五步"

#: ../../english/devel/join/nm-steps.inc:15
msgid "Step 6"
msgstr "第六步"

#: ../../english/devel/join/nm-steps.inc:16
msgid "Step 7"
msgstr "第七步"

#: ../../english/devel/join/nm-steps.inc:19
msgid "Applicants' checklist"
msgstr "申請者自我檢查單"

#: ../../english/devel/website/tc.data:11
msgid ""
"See <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/french/</a> "
"(only available in French) for more information."
msgstr ""
"詳細資訊請參考 <a href=\"m4_HOME/intl/french/\">https://www.debian.org/intl/"
"french/</a> (只有法語)。"

#: ../../english/devel/website/tc.data:12
#: ../../english/devel/website/tc.data:14
#: ../../english/devel/website/tc.data:15
#: ../../english/devel/website/tc.data:16
#: ../../english/events/merchandise.def:145
msgid "More information"
msgstr "更多信息"

#: ../../english/devel/website/tc.data:13
msgid ""
"See <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/spanish/</"
"a> (only available in Spanish) for more information."
msgstr ""
"詳細資訊請參考 <a href=\"m4_HOME/intl/spanish/\">https://www.debian.org/intl/"
"spanish/</a> (只有西班牙語)。"

#: ../../english/distrib/pre-installed.defs:18
msgid "Phone"
msgstr "電話"

#: ../../english/distrib/pre-installed.defs:19
msgid "Fax"
msgstr "傳真"

#: ../../english/distrib/pre-installed.defs:21
msgid "Address"
msgstr "地址"

#: ../../english/events/merchandise.def:13
msgid "Products"
msgstr "產品"

#: ../../english/events/merchandise.def:16
msgid "T-shirts"
msgstr "T-shirts"

#: ../../english/events/merchandise.def:19
msgid "hats"
msgstr "帽"

#: ../../english/events/merchandise.def:22
msgid "stickers"
msgstr "貼紙"

#: ../../english/events/merchandise.def:25
msgid "mugs"
msgstr "馬克杯"

#: ../../english/events/merchandise.def:28
msgid "other clothing"
msgstr "其他衣著"

#: ../../english/events/merchandise.def:31
msgid "polo shirts"
msgstr "polo 衫"

#: ../../english/events/merchandise.def:34
msgid "frisbees"
msgstr "[CN:飛盤:][HKTW:飛碟:]"

#: ../../english/events/merchandise.def:37
msgid "mouse pads"
msgstr "滑鼠墊"

#: ../../english/events/merchandise.def:40
msgid "badges"
msgstr "徽章"

#: ../../english/events/merchandise.def:43
msgid "basketball goals"
msgstr "篮球框"

#: ../../english/events/merchandise.def:47
msgid "earrings"
msgstr "耳環"

#: ../../english/events/merchandise.def:50
msgid "suitcases"
msgstr "手提箱"

#: ../../english/events/merchandise.def:53
msgid "umbrellas"
msgstr "雨伞"

#: ../../english/events/merchandise.def:56
msgid "pillowcases"
msgstr "枕套"

#: ../../english/events/merchandise.def:59
msgid "keychains"
msgstr "钥匙环"

#: ../../english/events/merchandise.def:62
msgid "Swiss army knives"
msgstr "瑞士军刀"

#: ../../english/events/merchandise.def:65
msgid "USB-Sticks"
msgstr "U 盘"

#: ../../english/events/merchandise.def:80
msgid "lanyards"
msgstr "项带"

#: ../../english/events/merchandise.def:83
msgid "others"
msgstr "其它"

#: ../../english/events/merchandise.def:90
msgid "Available languages:"
msgstr ""

#: ../../english/events/merchandise.def:107
msgid "International delivery:"
msgstr ""

#: ../../english/events/merchandise.def:118
msgid "within Europe"
msgstr ""

#: ../../english/events/merchandise.def:122
msgid "Original country:"
msgstr ""

#: ../../english/events/merchandise.def:187
msgid "Donates money to Debian"
msgstr "向 Debian 捐献"

#: ../../english/events/merchandise.def:192
msgid "Money is used to organize local free software events"
msgstr "财物将被用来组织本地的自由软件活动"

#: ../../english/logos/index.data:6
msgid "With&nbsp;``Debian''"
msgstr "包含&nbsp;``Debian''"

#: ../../english/logos/index.data:9
msgid "Without&nbsp;``Debian''"
msgstr "不包含&nbsp;``Debian''"

#: ../../english/logos/index.data:12
msgid "Encapsulated PostScript"
msgstr "精簡 PostScript"

#: ../../english/logos/index.data:18
msgid "[Powered by Debian]"
msgstr "[Powered by Debian]"

#: ../../english/logos/index.data:21
msgid "[Powered by Debian GNU/Linux]"
msgstr "[Powered by Debian GNU/Linux]"

#: ../../english/logos/index.data:24
msgid "[Debian powered]"
msgstr "[Debian powered]"

#: ../../english/logos/index.data:27
msgid "[Debian] (mini button)"
msgstr "[Debian] (mini button)"

#: ../../english/mirror/submit.inc:7
msgid "same as the above"
msgstr "同上"

#: ../../english/women/profiles/profiles.def:24
msgid "How long have you been using Debian?"
msgstr "您使用 Debian 多长时间了？"

#: ../../english/women/profiles/profiles.def:27
msgid "Are you a Debian Developer?"
msgstr "您是否为 Debian 开发者？"

#: ../../english/women/profiles/profiles.def:30
msgid "What areas of Debian are you involved in?"
msgstr "您参与了 Debian 中的那些方面？"

#: ../../english/women/profiles/profiles.def:33
msgid "What got you interested in working with Debian?"
msgstr "您为什么对在 Debian 中工作产生了兴趣？"

#: ../../english/women/profiles/profiles.def:36
msgid ""
"Do you have any tips for women interested in getting more involved with "
"Debian?"
msgstr "您是否有鼓励女性更多参与到 Debian 中的建议？"

#: ../../english/women/profiles/profiles.def:39
msgid ""
"Are you involved with any other women in technology group? Which one(s)?"
msgstr "您是否认识其它位于技术团体中的女性？您能否给出名称？"

#: ../../english/women/profiles/profiles.def:42
msgid "A bit more about you..."
msgstr "关于您的更多信息..."

#~ msgid "Wanted:"
#~ msgstr "希望："

#~ msgid "Who:"
#~ msgstr "誰："

#~ msgid "Architecture:"
#~ msgstr "硬體架構："

#~ msgid "Specifications:"
#~ msgstr "詳細資訊："

#~ msgid "Where:"
#~ msgstr "地點："

#~ msgid "Debian Technical Committee only"
#~ msgstr "限於 Debian 技術委員會"

#~ msgid "deity developers only"
#~ msgstr "限於 deity 開發者"

#~ msgid "developers only"
#~ msgstr "限於開發者"

#~ msgid "closed"
#~ msgstr "關閉"

#~ msgid "open"
#~ msgstr "公開"

#~ msgid "Mailing List Unsubscription"
#~ msgstr "取消訂閱郵件列表"

#~ msgid ""
#~ "Please respect the <a href=\"./#ads\">Debian mailing list advertising "
#~ "policy</a>."
#~ msgstr "請尊重 <a href=\"./#ads\">Debian 郵件列表廣告政策</a>。"

#~ msgid "Clear"
#~ msgstr "清除"

#~ msgid "Your E-Mail address:"
#~ msgstr "您的 E-Mail 地址："

#~ msgid "Mailing List Subscription"
#~ msgstr "訂閱郵件列表"

#~ msgid ""
#~ "<total_consultant> Debian consultants listed in <total_country> countries "
#~ "worldwide."
#~ msgstr ""
#~ "以下的名單中共有來自世界 <total_country> 個國家的 <total_consultant> 個 "
#~ "Debian 顧問。"

#~ msgid "Rates:"
#~ msgstr "收費："

#~ msgid "Email:"
#~ msgstr "Email："

#~ msgid "or"
#~ msgstr "或"

#~ msgid "URL:"
#~ msgstr "網址："

#~ msgid "Company:"
#~ msgstr "公司："

#~ msgid "Name:"
#~ msgstr "姓名："

#~ msgid "URL"
#~ msgstr "網址"

#~ msgid "Version"
#~ msgstr "版本"

#~ msgid "Status"
#~ msgstr "狀況"

#~ msgid "Package"
#~ msgstr "軟件套件"

#~ msgid "ALL"
#~ msgstr "全部"

#~ msgid "Unknown"
#~ msgstr "[CN:未知:][HKTW:不詳:]"

#~ msgid "??"
#~ msgstr "？？"

#~ msgid "BAD?"
#~ msgstr "不合格?"

#~ msgid "OK?"
#~ msgstr "合格?"

#~ msgid "BAD"
#~ msgstr "不合格"

#~ msgid "OK"
#~ msgstr "合格"

#~ msgid "Old banner ads"
#~ msgstr "舊的橫幅廣告"

#~ msgid "Download"
#~ msgstr "下載"

#~ msgid "Unavailable"
#~ msgstr "未提供"

#~ msgid "<void id=\"d-i\" />Unknown"
#~ msgstr "<void id=\"d-i\" />未知"

#~ msgid "No images"
#~ msgstr "沒有映像檔"

#~ msgid "No kernel"
#~ msgstr "沒有核心"

#~ msgid "Not yet"
#~ msgstr "尚未"

#~ msgid "Building"
#~ msgstr "可以建置"

#~ msgid "Booting"
#~ msgstr "可開機"

#~ msgid "sarge (broken)"
#~ msgstr "sarge (損毀)"

#~ msgid "sarge"
#~ msgstr "sarge"

#~ msgid "Working"
#~ msgstr "可用的"

msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2019-04-26 14:31-0400\n"
"Last-Translator: Boyuan Yang <073plan@gmail.com>\n"
"Language-Team: Debian-user in Chinese [Big5] <debian-chinese-big5@lists."
"debian.org>\n"
"Language: zh\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "委任的郵件"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "指名的郵件"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "委任"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "委任"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "委任"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "委任"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "委任"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "目前"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "會員"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "管理員"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "稳定版釋出管理员"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "巫師"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "主席"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "助理"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "秘書"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "總管"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "發行版"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "数据保护团队"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "宣传团队"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "支援以及下級組織"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "領袖"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "技術委員會"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "秘書"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "各項開發計劃"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP 庫存"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP 總管"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP 助理"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "釋出管理"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "釋出團隊"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "品質管理"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "安裝系統小組"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "釋出公告"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "光碟映像"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "產品"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "測試"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "云服务团队"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "自动构建架构"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Wanna-build 团队"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Buildd 管理"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "文档"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "需要幫助的套件清單"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "網頁"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Debian 星球"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Debian Women 計劃"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "活動"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "DebConf 委員會"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "合作計劃"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "硬體捐贈协调"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr "GNOME 基金會"

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "錯誤追縱系統"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "郵件列表管理與郵件列表庫存"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "新進開發人員前台"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debian 帳號管理員"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Keyring (PGP 與 GPG) 管理員"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "安全團隊"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "政策"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "系統管理"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need apackage installed."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"If you have hardware problems with Debian machines, please see<a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page,it should "
"contain per-machine administrator information."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP 開發人員目錄管理"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "鏡像站台"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS 管理"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "套件追蹤系統"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Salsa 管理员"

#~ msgid "Anti-harassment"
#~ msgstr "反骚扰"

#~ msgid "User support"
#~ msgstr "[CNHK:用戶:][TW:使用者:][HKTW:支援:][CN:支持:]"

#~ msgid "Embedded systems"
#~ msgstr "嵌入式系統"

#~ msgid "Firewalls"
#~ msgstr "防火牆"

#~ msgid "Laptops"
#~ msgstr "筆記型電腦"

#~ msgid "Special Configurations"
#~ msgstr "特別[CN:配置:][HKTW:組態設定:]"

#~ msgid "Ports"
#~ msgstr "移植"

#~ msgid "CD Vendors Page"
#~ msgstr "CD 廠商網頁"

#~ msgid "Consultants Page"
#~ msgstr "顧問網頁"

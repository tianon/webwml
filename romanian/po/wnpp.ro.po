msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-09-20 12:27+0100\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.8\n"

#: ../../english/template/debian/wnpp.wml:8
msgid "No requests for adoption"
msgstr "Fără solicitări pentru adopție"

#: ../../english/template/debian/wnpp.wml:12
msgid "No orphaned packages"
msgstr "Fără pachete orfane"

#: ../../english/template/debian/wnpp.wml:16
msgid "No packages waiting to be adopted"
msgstr "Fără pachete în așteptare de adopție"

#: ../../english/template/debian/wnpp.wml:20
msgid "No packages waiting to be packaged"
msgstr "Fără pachete în așteptare de împachetare"

#: ../../english/template/debian/wnpp.wml:24
msgid "No Requested packages"
msgstr "Nici un pachet solicitat"

#: ../../english/template/debian/wnpp.wml:28
msgid "No help requested"
msgstr "Nici un ajutor solicitat"

#. define messages for timespans
#. first the ones for being_adopted (ITAs)
#: ../../english/template/debian/wnpp.wml:34
msgid "in adoption since today."
msgstr "în adopție de azi"

#: ../../english/template/debian/wnpp.wml:38
msgid "in adoption since yesterday."
msgstr "în adopție de ieri"

#: ../../english/template/debian/wnpp.wml:42
msgid "%s days in adoption."
msgstr "%s zile în adopție."

#: ../../english/template/debian/wnpp.wml:46
msgid "%s days in adoption, last activity today."
msgstr "%s zile în adopție, ultima activitate azi.h"

#: ../../english/template/debian/wnpp.wml:50
msgid "%s days in adoption, last activity yesterday."
msgstr "%s zile în adopție, ultima activitate ieri"

#: ../../english/template/debian/wnpp.wml:54
msgid "%s days in adoption, last activity %s days ago."
msgstr "%s zile în adopție, ultima activitate acum %s zile."

#. timespans for being_packaged (ITPs)
#: ../../english/template/debian/wnpp.wml:60
msgid "in preparation since today."
msgstr "în pregătire de azi."

#: ../../english/template/debian/wnpp.wml:64
msgid "in preparation since yesterday."
msgstr "în pregătire de ieri."

#: ../../english/template/debian/wnpp.wml:68
msgid "%s days in preparation."
msgstr "%s zile în pregătire"

#: ../../english/template/debian/wnpp.wml:72
msgid "%s days in preparation, last activity today."
msgstr "%s zile în pregătire, ultima activitate azi."

#: ../../english/template/debian/wnpp.wml:76
msgid "%s days in preparation, last activity yesterday."
msgstr "%s zile în pregătire, ultima activitate ieri."

#: ../../english/template/debian/wnpp.wml:80
msgid "%s days in preparation, last activity %s days ago."
msgstr "%s zile în pregatire, ultima activitate acum %s zile."

#. timespans for request for adoption (RFAs)
#: ../../english/template/debian/wnpp.wml:85
msgid "adoption requested since today."
msgstr "adopție solicitată azi"

#: ../../english/template/debian/wnpp.wml:89
msgid "adoption requested since yesterday."
msgstr "adopție solicitată ieri"

#: ../../english/template/debian/wnpp.wml:93
msgid "adoption requested since %s days."
msgstr "adopție solicitată acum %s zile."

#. timespans for orphaned packages (Os)
#: ../../english/template/debian/wnpp.wml:98
msgid "orphaned since today."
msgstr "orfan de azi"

#: ../../english/template/debian/wnpp.wml:102
msgid "orphaned since yesterday."
msgstr "orfan de ieri"

#: ../../english/template/debian/wnpp.wml:106
msgid "orphaned since %s days."
msgstr "orfan de %s zile."

#. time spans for requested (RFPs) and help requested (RFHs)
#: ../../english/template/debian/wnpp.wml:111
msgid "requested today."
msgstr "solicitat azi"

#: ../../english/template/debian/wnpp.wml:115
msgid "requested yesterday."
msgstr "solicitat ieri"

#: ../../english/template/debian/wnpp.wml:119
msgid "requested %s days ago."
msgstr "solicitat acum %s zile."

#: ../../english/template/debian/wnpp.wml:133
msgid "package info"
msgstr "informații despre pachet"

#. popcon rank for RFH bugs
#: ../../english/template/debian/wnpp.wml:139
msgid "rank:"
msgstr "cotare:"

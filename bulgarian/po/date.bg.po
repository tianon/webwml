#
# Damyan Ivanov <dmn@debian.org>, 2010, 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: date\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: 2010-12-16 22:41+0300\n"
"PO-Revision-Date: 2011-01-14 07:31+0200\n"
"Last-Translator: Damyan Ivanov <dmn@debian.org>\n"
"Language-Team: Bulgarian <dict@fsa-bg.org>\n"
"Language: bg\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#. List of weekday names (used in modification dates)
#: ../../english/template/debian/ctime.wml:11
msgid "Sun"
msgstr "Нд"

#: ../../english/template/debian/ctime.wml:12
msgid "Mon"
msgstr "Пн"

#: ../../english/template/debian/ctime.wml:13
msgid "Tue"
msgstr "Вт"

#: ../../english/template/debian/ctime.wml:14
msgid "Wed"
msgstr "Ср"

#: ../../english/template/debian/ctime.wml:15
msgid "Thu"
msgstr "Чт"

#: ../../english/template/debian/ctime.wml:16
msgid "Fri"
msgstr "Пт"

#: ../../english/template/debian/ctime.wml:17
msgid "Sat"
msgstr "Сб"

#. List of month names (used in modification dates, and may be used in news 
#. listings)
#: ../../english/template/debian/ctime.wml:23
msgid "Jan"
msgstr "Яну"

#: ../../english/template/debian/ctime.wml:24
msgid "Feb"
msgstr "Фев"

#: ../../english/template/debian/ctime.wml:25
msgid "Mar"
msgstr "Мар"

#: ../../english/template/debian/ctime.wml:26
msgid "Apr"
msgstr "Апр"

#: ../../english/template/debian/ctime.wml:27
msgid "May"
msgstr "Май"

#: ../../english/template/debian/ctime.wml:28
msgid "Jun"
msgstr "Юни"

#: ../../english/template/debian/ctime.wml:29
msgid "Jul"
msgstr "Юли"

#: ../../english/template/debian/ctime.wml:30
msgid "Aug"
msgstr "Авг"

#: ../../english/template/debian/ctime.wml:31
msgid "Sep"
msgstr "Сеп"

#: ../../english/template/debian/ctime.wml:32
msgid "Oct"
msgstr "Окт"

#: ../../english/template/debian/ctime.wml:33
msgid "Nov"
msgstr "Ное"

#: ../../english/template/debian/ctime.wml:34
msgid "Dec"
msgstr "Дек"

#. List of long month names (may be used in "spoken" dates and date ranges).
#: ../../english/template/debian/ctime.wml:39
msgid "January"
msgstr "Януари"

#: ../../english/template/debian/ctime.wml:40
msgid "February"
msgstr "Февруари"

#: ../../english/template/debian/ctime.wml:41
msgid "March"
msgstr "Март"

#: ../../english/template/debian/ctime.wml:42
msgid "April"
msgstr "Април"

#. The <void> tag is to distinguish short and long forms of May.
#. Do not put it in msgstr.
#: ../../english/template/debian/ctime.wml:45
msgid "<void id=\"fullname\" />May"
msgstr "Май"

#: ../../english/template/debian/ctime.wml:46
msgid "June"
msgstr "Юни"

#: ../../english/template/debian/ctime.wml:47
msgid "July"
msgstr "Юли"

#: ../../english/template/debian/ctime.wml:48
msgid "August"
msgstr "Август"

#: ../../english/template/debian/ctime.wml:49
msgid "September"
msgstr "Септември"

#: ../../english/template/debian/ctime.wml:50
msgid "October"
msgstr "Октомври"

#: ../../english/template/debian/ctime.wml:51
msgid "November"
msgstr "Ноември"

#: ../../english/template/debian/ctime.wml:52
msgid "December"
msgstr "Декември"

#. $dateform: Date format (sprintf) for modification dates.
#. Available variables are: $mday = day-of-month, $monnr = month number,
#. $mon = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:60
msgid ""
"q{[%]s, [%]s [%]2d [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mon, $mday, "
"$hour, $min, $sec, q{UTC}, 1900+$year"
msgstr ""
"q{[%]s, [%]2d [%]s [%]02d:[%]02d:[%]02d [%]s [%]04d}, $wday, $mday, $mon, "
"$hour, $min, $sec, q{UTC}, 1900+$year"

#. $newsdateform: Date format (sprintf) for news items.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @moy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:68
msgid "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]04d}, $mday, $mon_str, $year"

#. $spokendateform: Date format (sprintf) for "spoken" dates
#. (such as the current release date).
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:79
msgid "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"
msgstr "q{[%]02d [%]s [%]d}, $mday, $mon_str, $year"

#. $spokendateform_noyear: Date format (sprintf) for "spoken" dates
#. (such as the current release date), without the year.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $mday = day-of-month, $mon = month number,
#. $mon_str = month string (from @longmoy).
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:90
msgid "q{[%]d [%]s}, $mday, $mon_str"
msgstr "q{[%]d [%]s}, $mday, $mon_str"

#. $spokendateform_noday: Date format (sprintf) for "spoken" dates
#. (such a conference event), without the day.
#. Available variables are: $mon = month number,
#. $mon_str = month string (from @longmoy), $year = year number.
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:99
msgid "q{[%]s [%]s}, $mon_str, $year"
msgstr "q{[%]s [%]s}, $mon_str, $year"

#. $rangeform_samemonth: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges within the same month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday = end
#. day-of-month, $smon = month number, $smon_str = month string (from @longmoy)
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:110
msgid "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"
msgstr "q{[%]d-[%]d [%]s}, $sday, $eday, $smon_str"

#. $rangeform_severalmonths: Date format (sprintf) for date ranges
#. (used mainly for events pages), for ranges spanning the end of a month.
#. Any special cases (such as the st/nd/rd/th suffixes in English) are
#. handled in the spokendate subroutine below.
#. Available variables are: $sday = start day-of-month, $eday, end
#. day-of-month, $smon = start month number, $emon = end month number,
#. $smon_str = start month string (from @longmoy), $emon_str = end month string
#. Percent signs are escaped because they are special during pass 2,
#. replace all % by [%]
#: ../../english/template/debian/ctime.wml:122
msgid "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"
msgstr "q{[%]d [%]s-[%]d [%]s}, $sday, $smon_str, $eday, $emon_str"

#use wml::debian::translation-check translation="b814e5f8f89e2086e558034fbf26a53d5f985512"
<define-tag pagetitle>Handshake donerar 300 000 USD till Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news
# $Id$

<p>
Debianprojektet mottog 2018 en donation på 300 000 USD från
<a href="https://handshake.org/">Handshake</a>, en organisation som utvecklar
ett experimentellt peer-to-peer rootdomännamngivningssystem.
</p>

<p>
Detta betydande ekonomiska bidrag kommer att hjälpa Debian att fortsätta
sin hårdvaruersättningsplan som utformats av
<a href="https://wiki.debian.org/Teams/DSA">Debians Systemadministratörer</a>,
som förnyar servrar och andra hårdvarukomponenter och därmed gör projektets
utvecklings- och gemenskapsinfrastruktur mer pålitlig.
</p>

<p><q><em>Ett djupt och uppriktigt tack till Handshake Foundation för ert
stöd till Debian-projektet</em></q> säger Chris Lamb, Debians projektledare.
<q><em>Bidrag som detta gör det möjligt för ett stort antal olika
bidragslämnare från hela världen att arbeta tillsammans med våra ömsesidiga
mål att utveckla ett helt fritt <q>universellt</q> operativsystem.</em></q>
</p>

<p>
Handshake är ett decentraliserat, tillståndsfritt namnprotokoll som är
kompatibelt med DNS där varje peer validerar och ansvarar för hanteringen
av rotzonen med målet att skapa ett alternativ till existerande
Certifikatutfärdare.
</p>

<p>
Handshakeprojektet, dess sponsorer och bidragslämnare erkänner Fri och
öppen mjukvara som en avgörande del av grunden för Internet och deras
projekt, så dom beslutade sig för att återinvestera tillbaka till
fri mjukvara genom att ge 10.2 miljoner USD till olika FLOSS-utvecklare och
-projekt, såväl som ideella organisationer och universitet som stöder fri
mjukvaruutveckling.
</p>

<p>
Ett stort tack, Handshake, för ert stöd!
</p>

<h2>Om Debian</h2>
<p>
Debianprojektet är en sammanslutning av utvecklare av fri mjukvara som
ger frivilligt av sin tid och insats för att producera det helt fria
operativsystemet Debian.
</p>

<h2>Kontaktinformation</h2>
<p>För ytterligare information, var vänlig besök Debians webbplats på
<a href="$(HOME)/">http://www.debian.org/</a> eller skicka e-post till
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

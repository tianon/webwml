#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>Uppdaterad Debian 10; 10.2 utgiven</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin andra uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>Dom som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på dom vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction aegisub "Fix crash when selecting a language from the bottom of the <q>Spell checker language</q> list; fix crash when right-clicking in the subtitles text box">
<correction akonadi "Fix various crashes / deadlock issues">
<correction base-files "Update /etc/debian_version for the point release">
<correction capistrano "Fix failure to remove old releases when there were too many">
<correction cron "Stop using obsolete SELinux API">
<correction cyrus-imapd "Fix data loss on upgrade from version 3.0.0 or earlier">
<correction debian-edu-config "Handle newer Firefox ESR configuration files; add post-up stanza to /etc/network/interfaces eth0 entry conditionally">
<correction debian-installer "Fix unreadable fonts on hidpi displays in netboot images booted with EFI">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction distro-info-data "Add Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter "New upstream stable release; fix sysvinit support; catch more ASCII encoding errors to improve resilience against bad data; fix message extraction so that signing in the same pass through the milter as verifying works correctly">
<correction emacs "Update the EPLA packaging key">
<correction fence-agents "Fix incomplete removal of fence_amt_ws">
<correction flatpak "New upstream stable release">
<correction flightcrew "Security fixes [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk "Fix over-aggressive font selection of Noto CJK fonts in modern web browsers under Chinese locale">
<correction freetype "Properly handle phantom points for variable hinted fonts">
<correction gdb "Rebuild against new libbabeltrace, with higher version number to avoid conflict with earlier upload">
<correction glib2.0 "Ensure libdbus clients can authenticate with a GDBusServer like the one in ibus">
<correction gnome-shell "New upstream stable release; fix truncation of long messages in Shell-modal dialogs; avoid crash on reallocation of dead actors">
<correction gnome-sound-recorder "Fix crash when selecting a recording">
<correction gnustep-base "Disable gdomap daemon that was accidentally enabled on upgrades from stretch">
<correction graphite-web "Remove unused <q>send_email</q> function [CVE-2017-18638]; avoid hourly error in cron when there is no whisper database">
<correction inn2 "Fix negotiation of DHE ciphersuites">
<correction libapache-mod-auth-kerb "Fix use after free bug leading to crash">
<correction libdate-holidays-de-perl "Mark International Childrens Day (Sep 20th) as a holiday in Thuringia from 2019 onwards">
<correction libdatetime-timezone-perl "Update included data">
<correction libofx "Fix null pointer dereference issue [CVE-2019-9656]">
<correction libreoffice "Fix the postgresql driver with PostgreSQL 12">
<correction libsixel "Fix several security issues [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt "Fix dangling pointer in xsltCopyText [CVE-2019-18197]">
<correction lucene-solr "Disable obsolete call to ContextHandler in solr-jetty9.xml; fix Jetty permissions on SOLR index">
<correction mariadb-10.3 "New upstream stable release">
<correction modsecurity-crs "Fix PHP script upload rules [CVE-2019-13464]">
<correction mutter "New upstream stable release">
<correction ncurses "Fix several security issues [CVE-2019-17594 CVE-2019-17595] and other issues in tic">
<correction ndppd "Avoid world writable PID file, that was breaking daemon init scripts">
<correction network-manager "Fix file permissions for <q>/var/lib/NetworkManager/secret_key</q> and /var/lib/NetworkManager">
<correction node-fstream "Fix arbitrary file overwrite issue [CVE-2019-13173]">
<correction node-set-value "Fix prototype pollution [CVE-2019-10747]">
<correction node-yarnpkg "Force using HTTPS for regular registries">
<correction nx-libs "Fix regressions introduced in previous upload, affecting x2go">
<correction open-vm-tools "Fix memory leaks and error handling">
<correction openvswitch "Update debian/ifupdown.sh to allow setting-up the MTU; fix Python dependencies to use Python 3">
<correction picard "Update translations to fix crash with Spanish locale">
<correction plasma-applet-redshift-control "Fix manual mode when used with redshift versions above 1.12">
<correction postfix "New upstream stable release; work around poor TCP loopback performance">
<correction python-cryptography "Fix test suite failures when built against newer OpenSSL versions; fix a memory leak triggerable when parsing x509 certificate extensions like AIA">
<correction python-flask-rdf "Add Depends on python{3,}-rdflib">
<correction python-oslo.messaging "New upstream stable release; fix switch connection destination when a rabbitmq cluster node disappears">
<correction python-werkzeug "Ensure Docker containers have unique debugger PINs [CVE-2019-14806]">
<correction python2.7 "Fix several security issues [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota "Fix rpc.rquotad spinning at 100% CPU">
<correction rpcbind "Allow remote calls to be enabled at run-time">
<correction shelldap "Repair SASL authentications, add a 'sasluser' option">
<correction sogo "Fix display of PGP-signed e-mails">
<correction spf-engine "New upstream stable release; fix sysvinit support">
<correction standardskriver "Fix deprecation warning from config.RawConfigParser; use external <q>ip</q> command rather than deprecated <q>ifconfig</q> command">
<correction swi-prolog "Use HTTPS when contacting upstream pack servers">
<correction systemd "core: never propagate reload failure to service result; fix sync_file_range failures in nspawn containers on arm, ppc; fix RootDirectory not working when used in combination with User; ensure that access controls on systemd-resolved's D-Bus interface are enforced correctly [CVE-2019-15718]; fix StopWhenUnneeded=true for mount units; make MountFlags=shared work again">
<correction tmpreaper "Prevent breaking of systemd services that use PrivateTmp=true">
<correction trapperkeeper-webserver-jetty9-clojure "Restore SSL compatibility with newer Jetty versions">
<correction tzdata "New upstream release">
<correction ublock-origin "New upstream version, compatible with Firefox ESR68">
<correction uim "Resurrect libuim-data as a transitional package, fixing some issues after upgrades to buster">
<correction vanguards "New upstream stable release; prevent a reload of tor's configuration via SIGHUP causing a denial-of-service for vanguards protections">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction firefox-esr "[armel] No longer supportable due to nodejs build-dependency">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>



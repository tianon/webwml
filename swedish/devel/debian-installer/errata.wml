#use wml::debian::template title="Debian-Installer errata"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="6ca8bcdc8acee152020274b3ab9d0bfcbe15efe3" mindelta="1" maxdelta="1"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"

<h1>Kända problem i <humanversion /></h1>

<p>
Detta är en lista på kända problem i utgåvan <humanversion /> av
Debian Installer. Om du inte ser ditt problem i listan här, vänligen sänd in en
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installationsrapport</a>
som beskriver problemet.
</p>

<dl class="gloss">

    <dt>Trasigt räddningsläge med den grafiska installeraren</dt>

    <dd>Det har upptäckts under avbildningstestning för Bullseye RC 1 att
    räddningsläget verkar vara trasigt (<a href="https://bugs.debian.org/987377">#987377</a>).
    Utöver detta behöver räddningsetiketten i bannern justeras för Bullseye-temat.
    <br />
    <b>Status:</b> Under utredning för att korrigeras innan den första
    officiella utgåvan av Bullseye.</dd>

    <dt>amdgpu fastprogramvara (firmware) krävs för många AMD-grafikkort</dt>
    <dd>Det verkar finnas ett ökat behov att installera amdgpu-fastprogramvara
    (firmware) (genom det icke-fria paketet <code>firmware-and-graphics</code>)
    för att undvika en svart skärm vid uppstart av det installerade systemet.
    Med Bullseye RC1, och även med en installationsavbildning som inkluderar
    alla firmwarepaket detekterar inte installeraren behovet av den specifika
    komponenten.
    <br />
    <b>Status:</b> Under utredning, för att korrigeras innan den första
    officiella utgåvan av Bullseye.</dd>

	<dt>GNOME kan misslyckas att starta med vissa inställningar i virtuella
	maskiner.</dt>
	<dd>Det uppmärksammades under testning av avbildningar av Stretch Alpha 4
	att GNOME kan misslyckas att starta beroende på inställningen som används
	för virtuella maskiner. Det verkar som om det är ok att använda cirrus som
	emulerad videokrets.
	<br />
	<b>Status:</b> Under utredning.</dd>
	
	<dt>Skrivbordsinstallationer fungerar möjligen inte med hjälp av endast CD#1</dt>
	<dd>
		Tack vare utrymmesbrist på den första CDn, så får inte alla väntade delar
		av GNOME-skrivbordet plats på CD#1. Använd extra paket-källor (t.ex. en
		andra CD eller en nätverksspegel) för en framgångsrik installation,
		<br />
		<b>Status:</b> Det är osannolikt att fler insatser kommer göras för att få
		fler paket att få plats på CD#1.
	</dd>

	<dt>LUKS2 är inkompatibelt med GRUB's kryptodiskstöd</dt>
	<dd>Det upptäckts bara nyligen att GRUB inte har stöd för
		LUKS2. Detta betyder att användare som vill använda
		<tt>GRUB_ENABLE_CRYPTODISK</tt> och undvika en separat,
		icke krypterad <tt>/boot</tt>, inte kommer att göra detta
		(<a href="https://bugs.debian.org/927165">#927165</a>).
		Denna setup stöds inte i installeraren hursomhelst, men det
		vore förnuftigt att åtminstone dokumentera denna begränsning
		mer framträdande, och åtminstone ha möjligheten att välja
		LUKS1 under installationsprocessen.
	<br />
	<b>Status:</b> Några idéer har uttryckts i felrapporten. De ansvariga för cryptsetup har skrivit <a href="https://cryptsetup-team.pages.debian.net/cryptsetup/encrypted-boot.html">specifik dokumentation</a>.</dd>

<!-- things should be better starting with Jessie Beta 2...
	<dt>Stöd för GNU/kFreeBSD</dt>

	<dd>
		Installationsavbildningarna för GNU/kFreeBSD påverkas av några
		viktiga fel
		(<a href="https://bugs.debian.org/757985">#757985</a>,
		<a href="https://bugs.debian.org/757986">#757986</a>,
		<a href="https://bugs.debian.org/757987">#757987</a>,
		<a href="https://bugs.debian.org/757988">#757988</a>). Anpassarna skulle
		absolut kunna använda en hjälpande hand när det gäller att få
		installeraren i skick!
	</dd>
-->
	
<!-- kind of obsoleted by the first "important change" mentioned in the 20140813 announce..
	<dt>Tillgänglighet i det installerade systemet</dt>
	
	<dd>
		Även om funktionalitet för tillgänglighet används i under
		installationsprocessen, så kanske detta inte automatiskt är aktiverat i
		det installerade systemet.
	</dd>
-->
	

<!--
	<dt>Potentiella problem med UEFI-uppstart på amd64</dt>
	<dd>Det har rapporterats problem vid uppstart av Debian Installer i UEFI-läge
		på amd64-system. Vissa system startar inte säkert med hjälp av
		<code>grub-efi</code>, och andra visar problem med grafisk korruption när
		den visar den inledande installationssplashskärmen.
	<br />
		Om du stöter på något av dessa problem, vänligen skicka en felrapport och
		ge oss så mycket detaljer som möjligt, både om symptomen och om din
		hårdvara - detta assisterar teamet när dom fixar problemen. Som en
		temporär lösning, försök med att stänga av UEFI och installera med hjälp
		av <q>Legacy BIOS</q> eller <q>Fallback-läge</q> istället.
	<br />
		<b>Status:</b>Fler felrättningar kan komma i de olika punkt-utgåvorna av
		Wheezy.
	</dd>
-->

<!-- leaving this in for possible future use...
	<dt>i386: more than 32 mb of memory is needed to install</dt>
	<dd>
	The minimum amount of memory needed to successfully install on i386
	is 48 mb, instead of the previous 32 mb. We hope to reduce the
	requirements back to 32 mb later. Memory requirements may have
	also changed for other architectures.
	</dd>
-->

</dl>

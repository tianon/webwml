#use wml::debian::template title="Debian GNU/NetBSD -- Por que?" BARETITLE="yes" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/netbsd/menu.inc"
#use wml::debian::translation-check translation="a1cc1a182e1feb2d026ca05e6cc8963b3964e136"

<h1>Por que Debian GNU/NetBSD?</h1>

<ul>
<li>O NetBSD roda em hardware não suportado pelo Linux. Portar o Debian para
o kernel NetBSD aumenta o número de plataformas que podem rodar um
sistema operacional baseado em Debian.</li>

<li>O projeto Debian GNU/Hurd demonstra que o Debian não está amarrado
a um kernel específico. Entretanto, o kernel Hurd ainda está relativamente
imaturo - o sistema Debian GNU/NetBSD seria utilizável em um nível
produtivo.</li>

<li>As lições aprendidas a partir do porte do Debian para o NetBSD podem ser
usadas no porte do Debian para outros kernels (como aqueles do <a
href="https://www.freebsd.org/">FreeBSD</a> e <a
href="http://www.openbsd.org/">OpenBSD</a>).</li>

<li>Em contraste com projetos como <a href="http://www.finkproject.org/">Fink</a>
ou <a href="http://debian-cygwin.sf.net/">Debian GNU/w32</a>, o Debian
GNU/NetBSD não existe para fornecer software extra ou um ambiente
estilo Unix para um SO existente (as árvores dos portes *BSD já são
abrangentes e elas sem dúvidas fornecem um ambiente estilo Unix). Ao contrário,
um(a) usuário(a) ou um(a) administrador(a) acostumado(a) a um sistema
Debian mais tradicional deve se sentir confortável com um sistema
Debian GNU/NetBSD imediatamente e de forma competente em um relativo curto
período de tempo.</li>

<li>Nem todo mundo gosta da árvore de portes do *BSD ou do espaço de usuário(a)
*BSD (isto é uma preferência pessoal em vez de qualquer tipo de comentário
sobre qualidade). As distribuições Linux têm sido produzidas para fornecer
portes estilo *BSD ou um espaço de usuário(a) estilo *BSD para aqueles(as)
que gostam do ambiente de usuário(a) BSD, mas também desejam usar o kernel
Linux - o Debian GNU/NetBSD é a lógica reversa disso, permitindo que as
pessoas que gostem do espaço de usuário(a) GNU ou de um sistema de
empacotamento estilo Linux usem o kernel NetBSD.</li>

<li>Porque nós podemos.</li>
</ul>

## Local variables:
## sgml-default-doctype-name: "HTML"
## End:

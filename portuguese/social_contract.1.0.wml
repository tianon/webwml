#use wml::debian::template title="Contrato Social Debian, Versão 1.0" BARETITLE=true
#use wml::debian::translation-check translation="5477c3c791fabf2db59622d9eec1061b6f73aa57"

#  Documento original: contract.html
#  Autor(a)          : Manoj Srivastava ( srivasta@tiamat.datasync.com )
#  Criado em         : Wed Jul  2 12:47:56 1997

<p>
  Versão 1.0 ratificada em 5 de julho de 1997. Substituída pela
  <a href="social_contract">Versão 1.1</a>, ratificada em 26 de abril de 2004.
</p>

<p>O Projeto Debian, produtor do sistema Debian GNU/Linux, criou o
<strong>Contrato Social Debian</strong>. A
<a href="#guidelines">Definição Debian de Software Livre (DFSG)</a>, uma
parte do contrato, inicialmente designada como um conjunto de compromissos
públicos que concordamos em respeitar, foi adotada pela
comunidade de software livre como a base para a
<a href="https://opensource.org/docs/osd">Definição de Código Aberto</a>.

<hr>
<h2>"Contrato Social" perante a Comunidade de Software Livre</h2>
<ol>
   <li><p><strong>O Debian permanecerá 100% livre</strong>
     <p>Prometemos manter a distribuição Debian GNU/Linux constituída
	integralmente de software livre. Como há muitas definições de
	software livre, incluímos as definições que usamos para a
	identificação de software "<em>livre</em>" abaixo.
	Iremos apoiar nossos(as) usuários(as) que desenvolvem e executam software
	não livre sobre o Debian, mas nunca faremos o sistema depender
	de um item de software que não seja livre.</p>

   <li><strong>Iremos retribuir à comunidade do software livre</strong>
	<p>Quando escrevermos novos componentes do sistema Debian, nós
	o licenciaremos como software livre. Iremos fazer o melhor
	sistema que pudermos, de modo que software livre seja
	amplamente distribuído e usado. Iremos fornecer aos(às)
	<em>autores(as) originais</em> dos componentes usados por
	nosso sistema as correções de bugs, aperfeiçoamentos,
	pedidos de usuários, etc.</p>

   <li><p><strong>Não esconderemos problemas</strong>
	<p>Iremos manter nosso banco de dados de relatório de falhas
	aberto para a visualização pública todo o tempo. Relatórios
	submetidos pelos(as) usuários(as) ficarão imediatamente visíveis para
	todos os(as) outros(as).</p>

   <li><p><strong>Nossas prioridades são nossos(as) usuários(as) e o software livre</strong>
	<p>Seremos guiados pelas necessidades de nossos(as) usuários(as) e
	pela comunidade de software livre, colocando seus interesses em
	primeiro lugar em nossas prioridades. Apoiaremos as necessidades
	de nossos(as) usuários(as) para operação em muitos tipos diferentes de
	ambiente computacional. Não iremos fazer objeção a software
	comercial que queira rodar em sistemas Debian, e permitiremos a
	outros(as) criarem distribuições com valor agregado contendo tanto o
	Debian como software comercial, não sendo nenhuma taxa por nós
	cobrada. Para apoiar estes objetivos, forneceremos um sistema
	operacional de alta qualidade, 100% software livre, sem restrições legais
	que possam impedir estes tipos de uso.</p>

   <li><p><strong>Programas que não atendem a nossos padrões de software livre.</strong>
	<p>Reconhecemos que alguns de nossos(as) usuários(as) precisam usar
	programas que não atendem
	à <a href="#guidelines">Definição Debian de Software Livre</a>.
	Criamos as áreas "<tt>contrib</tt>" e "<tt>non-free</tt>" dentro de
	nossos repositórios de FTP para este software. Os softwares contidos
	nestes diretórios não são parte do sistema Debian embora estejam configurados
	para uso com o Debian. Encorajamos fornecedores de
	CDs a ler as licenças de pacotes de software nestes diretórios e
	determinarem se podem ser distribuídos em seus CDs. Desta forma,
	embora software não livre não seja parte do Debian, apoiamos
	seus usuários(as) e fornecemos infraestrutura (como nosso sistema de
	controle de bugs e listas de discussão) para pacotes de software
	não livre.</p>
</ol>
<hr>
<h2 id="guidelines">A Definição Debian de Software Livre (DFSG)</h2>
<ol>
   <li><p><strong>Redistribuição livre</strong>
     <p>A licença de um componente Debian não pode restringir nenhuma parte
     interessada de vendê-lo, ou distribuir o software como parte de uma
     distribuição agregada de software contendo programas de diversas fontes
     diferentes. A licença não pode exigir um royalty ou outra taxa por esta
     venda.</p>
   <li><p><strong>Código-fonte</strong>
     <p>O programa deve incluir código-fonte e deve permitir a distribuição em
     código-fonte, bem como em formato compilado.</p>
   <li><p><strong>Trabalhos derivados</strong>
   <p>A licença deve permitir modificações e trabalhos derivados, e deve permitir
   que estes sejam distribuídos sob a mesma licença que o trabalho original.</p>
   <li><p><strong>Integridade do código-fonte do(a) autor(a)</strong>
    <p>A licença pode restringir o código-fonte de ser distribuído de forma modificada
    <strong>somente</strong> se a licença permitir a distribuição de
    "<tt>arquivos patch</tt>" com o código-fonte, com o propósito de modificar o
    programa em tempo de compilação. A licença deve permitir explicitamente a
    distribuição de software compilado a partir do código-fonte modificado. A
    licença pode exigir que trabalhos derivados tenham um nome ou número de versão
    diferente do software original. (<em>Este é um meio-termo; o Grupo Debian
    encoraja todos(as) os(as) autores(as) a não restringir nenhum arquivo, fonte ou
    binário, de ser modificado</em>).</p>
   <li><p><strong>Não à discriminação contra pessoas ou grupos.</strong>
     <p>A licença não pode discriminar nenhuma pessoa ou grupo de pessoas.</p>
   <li><p><strong>Não à discriminação contra fins de utilização</strong>
    <p>A licença não pode restringir ninguém de fazer uso do programa para um
    fim específico. Por exemplo, ela não pode restringir o programa de ser usado
    no comércio, ou de ser usado para pesquisa genética.</p>
   <li><p><strong>Distribuição de licença</strong>
     <p>Os direitos atribuídos ao programa devem se aplicar a todos aqueles(as)
     para quem o programa é redistribuído, sem a necessidade de execução de uma
     licença adicional por aquelas pessoas.</p>
   <li><p><strong>A Licença não pode ser específica para o Debian</strong>
     <p>Os direitos atribuídos ao programa não podem depender do programa ser
     parte de um sistema Debian. Se o programa for extraído do Debian e usado ou
     distribuído sem o Debian, dentro dos termos da licença do programa, os mesmos
     direitos garantidos em conjunto ao sistema Debian deverão ser garantidos
     àqueles(as) que o utilizam.</p>
   <li><p><strong>A licença não deve contaminar outros softwares.</strong>
     <p>A licença não poderá colocar restrições em outro software que é distribuído
     juntamente com o software licenciado. Por exemplo, a licença não pode insistir
     que todos os outros programas distribuídos na mesma mídia sejam software livre.</p>
   <li><p><strong>Licenças exemplo</strong>
     <p>As licenças "<strong><a href="https://www.gnu.org/copyleft/gpl.html">GPL</a></strong>",
     "<strong><a href="https://opensource.org/licenses/BSD-3-Clause">BSD</a></strong>" e
     "<strong><a href="https://perldoc.perl.org/perlartistic.html">Artistic</a></strong>"
     são exemplos de licenças que consideramos "<em>livres</em>".
</ol>

<p><em>O conceito de declarar nosso "contrato social para a comunidade de
software livre" foi sugerido por Ean Schuessler. O rascunho deste
documento foi escrito por Bruce Perens, refinado por outros(as)
desenvolvedores(as) Debian durante uma conferência via e-mail que durou
um mês em Junho de 1997 e então
<a href="https://lists.debian.org/debian-announce/debian-announce-1997/msg00017.html">\
aceita</a> como uma política pública do Projeto Debian.</em></p>

<p><em>Mais tarde, Bruce Perens removeu as referências específicas ao
Debian da Definição Debian de Software Livre para criar a
<a href="https://opensource.org/docs/definition.php">&ldquo;Definição de
Código Aberto&rdquo;</a>.</em></p>

<p><em>Outras organizações podem fazer derivações deste documento.
Por favor, dê o crédito ao Projeto Debian se você fizer isso.</em>

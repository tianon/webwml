# translation of consultants.po to Arabic
# Isam Bayazidi <isam@bayazidi.net>, 2004, 2005.
# Ossama M. Khayat <okhayat@yahoo.com>, 2005.
# Med Amine <medeb@protonmail.com>, 2013-20.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-25 07:00+0000\n"
"Last-Translator: Med Amine <medeb@protonmail.com>\n"
"Language-Team: Arabic <debian-l10n-arabic@lists.debian.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 ? 4 : 5;\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/consultants/consultant.defs:6
msgid "Name:"
msgstr "الاسم:"

#: ../../english/consultants/consultant.defs:9
msgid "Company:"
msgstr "الشركة:"

#: ../../english/consultants/consultant.defs:12
msgid "Address:"
msgstr "العنوان:"

#: ../../english/consultants/consultant.defs:15
msgid "Contact:"
msgstr "المراسل:"

#: ../../english/consultants/consultant.defs:19
msgid "Phone:"
msgstr "الهاتف:"

#: ../../english/consultants/consultant.defs:22
msgid "Fax:"
msgstr "الفاكس:"

#: ../../english/consultants/consultant.defs:25
msgid "URL:"
msgstr "المسار:"

#: ../../english/consultants/consultant.defs:29
msgid "or"
msgstr "أو"

#: ../../english/consultants/consultant.defs:34
msgid "Email:"
msgstr "البريد الإلكتروني:"

#: ../../english/consultants/consultant.defs:52
msgid "Rates:"
msgstr "السّعر:"

#: ../../english/consultants/consultant.defs:55
msgid "Additional Information"
msgstr "معلومات إضافية"

#: ../../english/consultants/consultant.defs:58
msgid "Willing to Relocate"
msgstr ""

#: ../../english/consultants/consultant.defs:61
msgid ""
"<total_consultant> Debian consultants listed in <total_country> countries "
"worldwide."
msgstr ""
"<total_consultant> مستشارو دبيان المُسردون في <total_country> دولة حول العالم."

#: ../../english/template/debian/consultant.wml:6
msgid "List of Consultants"
msgstr "قائمة المستشارين"

#: ../../english/template/debian/consultant.wml:9
msgid "Back to the <a href=\"./\">Debian consultants page</a>."
msgstr "العودة إلى<a href=\"./\">صفحة المستشارين لدبيان</a>."

#use wml::debian::template title="Debian &ldquo;stretch&rdquo; Installation Information" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/stretch/release.data"
#use wml::debian::translation-check translation="73724899f30de2473094581fc7b72d9858bdb469" maintainer="galaxico"

<h1>Installing Debian <current_release_stretch></h1>

<if-stable-release release="buster">
<p><strong>Debian 9 has been superseded by
<a href="../../buster/">Debian 10 (<q>buster</q>)</a>. Some of these
installation images may no longer be available, or may no longer work, and
you are recommended to install buster instead.
</strong></p> 
</if-stable-release>

<p>
<strong>To install Debian</strong> <current_release_stretch>
(<em>stretch</em>), download any of the following images (all i386 and amd64
CD/DVD images can be used on USB sticks too):
</p>

<div class="line">
<div class="item col50">
	<p><strong>netinst CD image (generally 150-280 MB)</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>full CD sets</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>full DVD sets</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>other images (netboot, flexible usb stick, etc.)</strong></p>
<other-images />
</div>
</div>

<div id="firmware_nonfree" class="warning">
<p>
If any of the hardware in your system <strong>requires non-free firmware to be
loaded</strong> with the device driver, you can use one of the
<a href="https://cdimage.debian.org/cdimage/unofficial/non-free/firmware/stretch/current/">\
tarballs of common firmware packages</a> or download an <strong>unofficial</strong> image including these <strong>non-free</strong> firmwares. Instructions how to use the tarballs
and general information about loading firmware during an installation can
be found in the Installation Guide (see Documentation below).
</p>
<div class="line">
<div class="item col50">
<p><strong>netinst (generally 240-290 MB) <strong>non-free</strong>
CD images <strong>with firmware</strong></strong></p>
<small-non-free-cd-images />
</div>
</div>
</div>



<p>
<strong>Notes</strong>
</p>
<ul>
    <li>
	For downloading full CD and DVD images the use of BitTorrent or jigdo
	is recommended.
    </li><li>
	For the less common architectures only a limited number of images
	from the CD and DVD sets is available as ISO file or via BitTorrent.
	The full sets are only available via jigdo.
    </li><li>
	The multi-arch <em>CD</em> images support i386/amd64; the installation is similar to installing
	from a single architecture netinst image.
    </li><li>
	The multi-arch <em>DVD</em> image supports i386/amd64; the
	installation is similar to installing from a single architecture full
	CD image; the DVD also includes the source for all included packages.
    </li><li>
	For the installation images, verification files (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt> and other) are available from the same directory as the
	images.
    </li>
</ul>


<h1>Documentation</h1>

<p>
<strong>If you read only one document</strong> before installing, read our
<a href="../i386/apa">Installation Howto</a>, a quick
walkthrough of the installation process. Other useful documentation includes:
</p>

<ul>
<li><a href="../installmanual">Stretch Installation Guide</a><br />
detailed installation instructions</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian-Installer FAQ</a>
and <a href="$(HOME)/CD/faq/">Debian-CD FAQ</a><br />
common questions and answers</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer Wiki</a><br />
community maintained documentation</li>
</ul>

<h1 id="errata">Errata</h1>

<p>
This is a list of known problems in the installer shipped with 
Debian <current_release_stretch>. If you have experienced a problem
installing Debian and do not see your problem listed here, please send us an 
<a href="$(HOME)/releases/stable/i386/ch05s04.html#submit-bug">installation report</a>
describing the problem or 
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">check the wiki</a>
for other known problems.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata for release 9.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->

     <dt>pkgsel did not install updates with ABI changes (by default)</dt>

     <dd>Bug <a href="https://bugs.debian.org/908711">#908711</a>:
     during installation with network sources enabled, the
     installed security updates don't include updates that depend on a new
     binary package, due to a kernel or library ABI change.

     <br /> <b>Status:</b> This has been fixed in 9.6</dd>

     <dt>APT was vulnerable to a man-in-the-middle attack</dt>

     <dd>A bug in the APT HTTP transport method
     (<a href="https://www.debian.org/security/2019/dsa-4371">CVE-2019-3462</a>)
     could be exploited by an attacker located as a man-in-the-middle between APT
     and a mirror to cause the installation of additional, malicious, packages.

     <br /> This can be mitigated by disabling use of the network during
     initial installation and then upgrading following the instructions in
     <a href="$(HOME)/security/2019/dsa-4371">DSA-4371</a>.

     <br /> <b>Status:</b> This has been fixed in 9.7</dd>

</dl>

<if-stable-release release="stretch">
<p>
Improved versions of the installation system are being developed
for the next Debian release, and can also be used to install stretch.
For details, see 
<a href="$(HOME)/devel/debian-installer/">the Debian-Installer project
page</a>.
</p>
</if-stable-release>

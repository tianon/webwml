#use wml::debian::template title="Debian 11 -- Παροράματα" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="1b53acd1a1236a10ef76b706c36cf6495d8ebfcf" maintainer="galaxico"

#include "$(ENGLISHDIR)/releases/info"

<toc-display/>


# <toc-add-entry name="known_probs">Known problems</toc-add-entry>
<toc-add-entry name="security">Ζητήματα Ασφαλείας</toc-add-entry>

<p>Η Ομάδα Ασφαλείας του Debian εκδίδει επικαιροποιήσεις σε πακέτα στη σταθερή διανομή στην οποία έχει εντοπίσει προβλήματα σχετικά με την ασφάλεια. Παρακαλούμε συμβουλευθείτε
<a href="$(HOME)/security/">σελίδες ασφαλείας</a> για πληροφορίες σχετικά με οποιαδήποτε ζητήματα ασφαλείας εντοπίζονται για την έκδοση <q>bullseye</q>.</p>

<p>Αν χρησιμοποιείτε το APT, προσθέστε την ακόλουθη γραμμή στο αρχείο <tt>/etc/apt/sources.list</tt> για να μπορέσετε να έχετε πρόσβαση στις πιο πρόσφατες επικαιροποιήσεις ασφαλείας:</p>

<pre>
  deb http://security.debian.org/ bullseye-security main contrib non-free
</pre>

<p>Στη συνέχεια, τρέξτε την εντολή <kbd>apt update</kbd> ακολουθούμενη από την εντολή <kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="pointrelease">Σημειακές εκδόσεις</toc-add-entry>

<p>Μερικές φορές, στην περίπτωση αρκετών κρίσιμων προβλημάτων ή επικαιροποιήσεων ασφαλείας, η έκδοση που έχει κυκλοφορήσει επικαιροποιείται. Γενικά, αυτές οι επικαιροποιήσεις υποδεικνύονται ως "σημειακές" εκδόσεις.</p>

<!-- <ul>
  <li>The first point release, 11.1, was released on
      <a href="$(HOME)/News/2017/FIXME">FIXME</a>.</li>
</ul> -->

<ifeq <current_release_bullseye> 11.0 "

<p>Δεν υπάρχουν ακόμα σημειακές εκδόσεις για το Debian 11.</p>" "

<p>Δείτε το <a
href="http://http.us.debian.org/debian/dists/bullseye/ChangeLog">\
ChangeLog</a>
για λεπτομέρειες σχετικά με τις αλλαγές μεταξύ της έκδοσης 11 και της τρέχουσας έκδοσης <current_release_bullseye/>.</p>"/>


<p>Διορθώσεις στην σταθερή διανομή σε κυκλοφορία συνήθως περνάμε από μια εκτενή περίοδο δοκιμών και ελέγχου πριν γίνουν δεκτές την αρχειοθήκη. Όμως,αυτές οι διορθώσεις είναι διαθέσιμες στον κατάλογο
<a href="http://ftp.debian.org/debian/dists/bullseye-proposed-updates/">\
dists/bullseye-proposed-updates</a> οποιουδήποτε καθρέφτη της αρχειοθήκης του Debian.</p>

<p>Αν χρησιμοποιείτε το APT για την επικαιροποίηση των πακέτων σας, μπορείτε να προθέσετε τις προτεινόμενες επικαιροποιήσεις προσθέτοντας την παρακάτω γραμμή στο αρχείο 
<tt>/etc/apt/sources.list</tt>:</p>

<pre>
  \# proposed additions for a 11 point release
  deb http://deb.debian.org/debian bullseye-proposed-updates main contrib non-free
</pre>

<p>Μετά από αυτό, τρέξτε την εντολή <kbd>apt update</kbd> ακολουθούμενη από την εντολή <kbd>apt upgrade</kbd>.</p>


<toc-add-entry name="installer">Σύστημα εγκατάστασης</toc-add-entry>

<p>
Για πληροφορίες σχετικά με παροράματα και επικαιροποιήσεις του συστήματος εγκατάστασης, δείτε τη σελίδα 
the <a href="debian-installer/">πληροφορίες εγκατάστασης</a>.
</p>

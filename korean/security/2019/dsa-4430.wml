#use wml::debian::translation-check translation="8f89f4f84d1b72c6872f117662668a6e94dbb51a" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>Mathy Vanhoef (NYUAD) and Eyal Ronen (Tel Aviv University &amp; KU Leuven) found
multiple vulnerabilities in the WPA implementation found in wpa_supplication
(station) and hostapd (access point). These vulnerability are also collectively
known as <q>Dragonblood</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9495">CVE-2019-9495</a>

    <p>Cache-based side-channel attack against the EAP-pwd implementation: an
    attacker able to run unprivileged code on the target machine (including for
    example javascript code in a browser on a smartphone) during the handshake
    could deduce enough information to discover the password in a dictionary
    attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9497">CVE-2019-9497</a>

    <p>Reflection attack against EAP-pwd server implementation: a lack of
    validation of received scalar and elements value in the EAP-pwd-Commit
    messages could result in attacks that would be able to complete EAP-pwd
    authentication exchange without the attacker having to know the password.
    This does not result in the attacker being able to derive the session key,
    complete the following key exchange and access the network.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9498">CVE-2019-9498</a>

    <p>EAP-pwd server missing commit validation for scalar/element: hostapd
    doesn't validate values received in the EAP-pwd-Commit message, so an
    attacker could use a specially crafted commit message to manipulate the
    exchange in order for hostapd to derive a session key from a limited set of
    possible values. This could result in an attacker being able to complete
    authentication and gain access to the network.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9499">CVE-2019-9499</a>

    <p>EAP-pwd peer missing commit validation for scalar/element: wpa_supplicant
    doesn't validate values received in the EAP-pwd-Commit message, so an
    attacker could use a specially crafted commit message to manipulate the
    exchange in order for wpa_supplicant to derive a session key from a limited
    set of possible values. This could result in an attacker being able to
    complete authentication and operate as a rogue AP.</p>

</ul>

<p>Note that the Dragonblood moniker also applies to
<a href="https://security-tracker.debian.org/tracker/CVE-2019-9494">\
CVE-2019-9494</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2014-9496">\
CVE-2014-9496</a> which are vulnerabilities in the SAE protocol in WPA3. SAE is not
enabled in Debian stretch builds of wpa, which is thus not vulnerable by default.</p>

<p>Due to the complexity of the backporting process, the fix for these
vulnerabilities are partial. Users are advised to use strong passwords to
prevent dictionary attacks or use a 2.7-based version from stretch-backports
(version above 2:2.7+git20190128+0c1e29f-4).</p>

<p>안정 배포(stretch)에서 이 문제를 버전 2:2.4-1+deb9u3에서 수정했습니다.</p>

<p>wpa 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>wpa의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4430.data"

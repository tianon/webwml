#use wml::debian::template title="¿Cómo puede ayudar a Debian?"
#use wml::debian::translation-check translation="1335baf12e8840fa448cae80418e452e885d5e8a" maintainer="Laura Arjona Reina"

### TRANSLATORS: Revision 1.27 is adding some words to the popcon submission item,
### and Revision 1.28 is reorganization of items (no change in the description of
### each item, just adding title sections and per section unnumbered lists.
### see #872667 for details and the patches.

<p>Si está pensando cómo podría ayudar al desarrollo de 
Debian, debe tener en cuenta que hay muchas áreas en las
que tanto los usuarios expertos como los inexpertos pueden ayudar:</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>Programación</h3>

<ul>
<li>Puede empaquetar aplicaciones con las que tenga mucha experiencia
y que considere que podrían ser valiosas para Debian, convirtiéndose
en la persona que mantiene dichos paquetes. Consulte el <a
href="$(HOME)/devel/">rincón del desarrollador de Debian</a> para más
información.</li>
 <li>Puede ayudar a 
<a href="https://security-tracker.debian.org/tracker/data/report">llevar seguimiento</a>,
<a href="$(HOME)/security/audit/">encontrar</a> y
<a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">arreglar</a>
<a href="$(HOME)/security/">problemas de seguridad</a> en los paquetes de Debian.
También puede ayudar a fortalecer 
<a href="https://wiki.debian.org/Hardening">paquetes</a>,
<a href="https://wiki.debian.org/Hardening/RepoAndImages">repositorios e imágenes</a>
y <a href="https://wiki.debian.org/Hardening/Goals">otras cosas</a>.
</li>
<li>Puede ayudar manteniendo aplicaciones que están disponibles para
el sistema operativo Debian, especialmente aquellas
en las que tiene mucha experiencia y conocimientos, enviando correcciones
(parches) o información detallada de los fallos de los paquetes en el
<a href="https://bugs.debian.org/">sistema de seguimiento de fallos</a>.
También puede involucrarse directamente en el mantenimiento de los
paquetes si se convierte en miembro de un equipo de mantenimiento o
se involucra en el desarrollo de los programas que se realizan para
Debian uniéndose a un proyecto software en
<a href="https://salsa.debian.org/">Salsa</a>.</li>
<li>Puede ayudar a adaptar Debian a alguna arquitectura con la que
tenga experiencia, bien empezando una nueva adaptación, o contribuyendo
a adaptaciones que ya estén en curso. Consulte la 
<a href="$(HOME)/ports/">lista de adaptaciones disponibles</a>
si desea más información.</li>
<li>Puede ayudar a mejorar servicios <a href="https://wiki.debian.org/Services">existentes</a>
relacionados con Debian o crear nuevos servicios
<a href="https://wiki.debian.org/Services#wishlist">que se necesitan</a>
en la comunidad.
</ul>

<h3>Pruebas</h3>

<ul>
<li>Puede simplemente probar el sistema operativo y los programas que
encontrará en él e informar de cualquier errata o fallo que encuentre
(del cual aún no se haya informado), utilizando el
<a href="https://bugs.debian.org/">sistema de seguimiento de fallos</a>
(o BTS, <em>Bug Tracking System</em>). También puede intentar revisar los fallos que
se han encontrado en los programas que utiliza y proporcionar más 
información, si puede reproducir los problemas que se describen allí.</li>
<li>Puede ayudar <a href="https://wiki.debian.org/Teams/DebianCD/ReleaseTesting">probando el instalador y las imágenes ISO</a>,</li>
<a href="https://wiki.debian.org/SecureBoot/Testing">el soporte de arranque seguro</a>,
<a href="https://wiki.debian.org/LTS/TestSuites">las actualizaciones LTS</a>
y
<a href="https://wiki.debian.org/U-boot/Status">el cargador u-boot</a>.
</li>
</ul>

<h3>Ayuda al usuario</h3>
<ul>
# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<li>Si tiene experiencia en el uso de Debian puede ayudar a otros usuarios
a través de las <a href="$(HOME)/support#mail_lists">listas de correo
de usuarios</a> y, específicamente, a través de la <a
href="https://lists.debian.org/debian-user-spanish/">lista de usuarios de habla
hispana</a>, o a través del canal de IRC <tt>#debian</tt> (en inglés) y
<tt>#debian-es</tt> (en español). Consulte las 
<a href="$(HOME)/support">páginas de soporte</a> para obtener más información
sobre las distintas fuentes y opciones de soporte.</li>
</ul>

<h3>Traducción</h3>
<ul>
# TBD - link to translators mailing lists
# Translators, link directly to your group's pages
<li>Puede ayudar traduciendo aplicaciones e información relacionada
con Debian (páginas web, documentación, etc.) a su propio idioma si
se une al esfuerzo del proyecto de traducción. Las discusiones generales se
gestionan en la <a href="https://lists.debian.org/debian-i18n/">lista de i18n</a> y el proyecto de traducción al español se coordina en la lista
<a href="https://lists.debian.org/debian-l10n-spanish/">debian-l10n-spanish</a>.
Encontrará más información del proyecto de traducción de Debian
al español en <a href="$(HOME)/international/spanish/">las páginas
del proyecto</a>. Puede incluso comenzar un nuevo grupo de
internacionalización si no hay ningún grupo de su idioma. Lea las
<a href="$(HOME)/international/">páginas sobre internacionalización</a>
si desea más información.</li>
</ul>

<h3>Documentación</h3>
<ul>
<li>Puede ayudar escribiendo documentación, tanto si contribuye
a la documentación oficial que ofrece el
<a href="$(HOME)/doc/ddp">Proyecto de Documentación de Debian</a> como si
contribuye al <a href="https://wiki.debian.org/">Wiki de Debian</a>.</li>

<li>Puede etiquetar y categorizar paquetes en el sitio web <a href="https://debtags.debian.org/">debtags</a> 
para que nuestros usuarios puedan encontrar más fácilmente el software que buscan.
</li>
</ul>

<h3>Actos</h3>
<ul>
<li>Puede ayudar con la cara <em>pública</em> de Debian
colaborando en el desarrollo del <a href="$(HOME)/devel/website/">sitio web</a>
o ayudando en la organización de la realización de <a
href="$(HOME)/events/">actos</a> en cualquier parte del mundo.</li>

<li>Ayude a la promoción de Debian hablando de él o demostrándoselo a otros.</li>

<li>Ayude creando u organizando un <a href="https://wiki.debian.org/LocalGroups">grupo local de Debian</a>
con encuentros y/u otras actividades.</li>

<li>
Puede ayudar en la <a href="https://debconf.org/">conferencia Debian</a> anual
 <a href="https://wiki.debconf.org/wiki/Videoteam">grabando vídeos de las charlas</a>,
 <a href="https://wiki.debconf.org/wiki/FrontDesk">recibiendo a los asistentes cuando llegan</a>,
 <a href="https://wiki.debconf.org/wiki/Talkmeister">ayudando a los ponentes antes de las charlas</a>,
 en eventos especiales (como la fiesta de vino y queso), el montaje y desmontaje, y otras tareas.
</li>
<li>
Puede ayudar a organizar la <a href="https://debconf.org/">conferencia Debian</a> anual,
 mini-DebConfs en su región,
 <a href="https://wiki.debian.org/DebianDay">fiestas del día de Debian</a>,
 <a href="https://wiki.debian.org/ReleaseParty">fiestas de publicación</a>,
 <a href="https://wiki.debian.org/BSP">fiestas de corrección de fallos</a>,
 <a href="https://wiki.debian.org/Sprints">reuniones de desarrollo</a> y
 <a href="https://wiki.debian.org/DebianEvents">otros eventos</a> en todo el mundo.
</li>
</ul>

<h3>Hacer donaciones</h3>
<ul>
<li>Puede <a href="$(HOME)/donations">donar dinero, equipamiento o servicios</a>
al proyecto Debian para que los usuarios o los desarrolladores se puedan
beneficiar de estos. Estamos buscando constantemente
<a href="$(HOME)/mirror/">servidores réplica en todo el mundo</a> que puedan
utilizar nuestros usuarios, así como
<a href="$(HOME)/devel/buildd/">sistemas autocompiladores</a> para las personas
que realizan adaptaciones.</li>
</ul>

<h3>¡Usar Debian!</h3>
<ul>
<li>
Puede <a href="https://wiki.debian.org/ScreenShots">hacer pantallazos</a> de
paquetes y <a href="https://screenshots.debian.net/upload">subirlos</a> a
<a href="https://screenshots.debian.net/">screenshots.debian.net</a> para que
nuestros usuarios puedan ver el aspecto del software en Debian antes de usarlo.
</li>
<li>Puede habilitar los <a href="https://packages.debian.org/popularity-contest">\
envíos de «popularity-contest» (estadísticas de uso)</a> para que sepamos qué paquetes son populares y más útiles para los usuarios.</li>
</ul>

<p>Hay, como puede ver, muchas formas en las que puede involucrarse en el
proyecto y solo para algunas de estas es necesario ser un desarrollador de
Debian. Muchos de los proyectos existentes tienen mecanismos que permiten el
acceso directo a los árboles de código fuente a los colaboradores que han
demostrado que son valiosos y de confianza. Generalmente aquellas personas que
piensan que pueden involucrarse mucho más con Debian se
<a href="$(HOME)/devel/join">unirán al proyecto</a>, pero no es un
requisito imprescindible.</p>

<h3>Organizaciones</h3>

<p>
Su organización educativa, comercial, sin ánimo de lucro o gubernamental
puede estar interesada en utilizar sus recursos para ayudar a Debian.
Su organización puede
<a href="https://www.debian.org/donations">realizar una donación</a>,
<a href="https://www.debian.org/partners/">formalizar una relación de socio de Debian</a>,
<a href="https://www.debconf.org/sponsors/">patrocinar nuestras conferencias</a>,
<a href="https://wiki.debian.org/MemberBenefits">proporcionar productos o servicios gratuitos a los contribuidores de Debian</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">proporcionar alojamiento gratuito para servicios experimentales de Debian</a>,
gestionar réplicas de nuestro
<a href="https://www.debian.org/mirror/ftpmirror">software</a>,
<a href="https://www.debian.org/CD/mirroring/">medios de instalación</a>
o <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">vídeos de las conferencias</a>
o promocionar nuestro software y la comunidad 
<a href="https://www.debian.org/users/">proporcionando un testimonio de uso de Debian</a>
o vendiendo <a href="https://www.debian.org/events/merchandise">«merchandising»</a> de Debian,
<a href="https://www.debian.org/CD/vendors/">medios de instalación</a>,
<a href="https://www.debian.org/distrib/pre-installed">sistemas preinstalados</a>,
proporcionando servicios de <a href="https://www.debian.org/consultants/">consultoría</a> o
<a href="https://wiki.debian.org/DebianHosting">alojamiento</a>.
</p>

<p>
También puede ayudar animando a sus empleados o miembros a participar en nuestra comunidad
poniéndoles en contacto con Debian mediante el uso de nuestro sistema operativo en su organización,
realizando formación sobre el sistema operativo Debian y su comunidad, dirigiéndoles a contribuir durante
su tiempo de trabajo o enviándoles a nuestros <a href="$(HOME)/events/">actos</a>.
</p>

# <p>Related links:
